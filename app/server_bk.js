var express     = require('express');//server
var app         = express();
var http        = require('http').Server(app);
var io          = require('socket.io')(http);//sockets
var fs          = require('fs');//filesystem
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');//mongodb orm
var multer      = require('multer');//file uploads
var request     = require('request');//requests
var morgan      = require('morgan');//logging
var passport	  = require('passport');//authentication
var config      = require('./config/database'); // get db config file
var jwt         = require('jwt-simple');//json web tokens
var lwip        = require('lwip');//image processing
var htmlToArticleJson = require('html-to-article-json')({});
var port        = process.env.PORT || 8080;


//models
var Article = require('./models/Article.js');
var User = require('./models/User.js');
var Media = require('./models/Media.js');
var Comment = require('./models/Comment.js');
//end models

var options = {};

var searchIndex = require('search-index');
var data = require('./indexes/articles.json');


//use ejs as the view engine
app.set('view engine', 'ejs');

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Token");
  next();
});


function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    var mimetype = file.mimetype.toLowerCase();
    switch(mimetype) {
        case 'image/jpeg':
            extension = '.jpg';
            break;
        case 'image/gif':
            extension = '.gif';
            break;
        case 'image/png':
            extension = '.png';
            break;
        default:
            extension = '.jpg';
    }
    cb(null, guid() + Date.now() + extension)
    // cb(null, file.fieldname.replace(/\W+/g, '-').toLowerCase() + Date.now() + extension)
  }
});

app.use(multer({ storage: storage }).array('files'));


//connect to mongodb articles database
mongoose.connect('mongodb://localhost/article');
// Config body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//for serving static pages
/*
 __ _        _   _          ___
/ _\ |_ __ _| |_(_) ___    / _ \__ _  __ _  ___  ___
\ \| __/ _` | __| |/ __|  / /_)/ _` |/ _` |/ _ \/ __|
_\ \ || (_| | |_| | (__  / ___/ (_| | (_| |  __/\__ \
\__/\__\__,_|\__|_|\___| \/    \__,_|\__, |\___||___/
                                     |___/
*/
app.use(express.static('public'));
app.use(express.static('uploads'));
//end static pages


//log to the console
app.use(morgan('dev'));
// Use the passport package in our application
app.use(passport.initialize());


app.get('/', function(req, res){
  //do nothing
});





// pass passport for configuration
require('./config/passport')(passport);


// create a new user account
app.post('/v1/auth/signup', function(req, res) {
  console.log(req.body.user);
  if (!req.body.user.email || !req.body.user.password) {
    res.json({success: false, msg: 'Please enter email and password.'});
  } else {
    var newUser = new User(req.body.user);
    // save the user
    newUser.save(function(err) {
      if (err) {
        return res.json({success: false, msg: 'User already exists.'});
      }
      res.json({success: true, msg: 'Successful created new user.'});
    });
  }
});


//authenticate user to get token
app.post('/v1/auth/authenticate', function(req, res) {
  User.findOne({
    email: req.body.user.email
  }, function(err, user) {
    if (err)   return res.json({success: false, msg: err});

    if (!user) {
      res.send({success: false, msg: 'Authentication failed. User not found.'});
    } else {
      // check if password matches
      user.comparePassword(req.body.user.password, function (err, isMatch) {
        if (isMatch && !err) {
          // if user is found and password is right create a token
          var token = jwt.encode(user, config.secret);
          // return the information including token as JSON
          res.json({success: true, token: 'JWT ' + token});
        } else {
          res.send({success: false, msg: 'Authentication failed. Wrong password.'});
        }
      });
    }
  });
});


getToken = function (headers) {
  // console.log(headers.cookie)
  // console.log(headers.token)
  console.log("headers: "+JSON.stringify(headers))
  console.log("token: "+headers.token)
  if (headers && headers.token) {
  // if (headers && headers.cookie) {
    var parted = headers.token.split('%20');
    var parted = headers.token.split(' ');
    if (parted.length === 2) {
      return parted[1];
    } else {
      return null;
    }
  } else {
    return null;
  }
};



//CRUD ROUTES
/*
             _                   _
  __ _ _ __ (_)  _ __ ___  _   _| |_ ___  ___
 / _` | '_ \| | | '__/ _ \| | | | __/ _ \/ __|
| (_| | |_) | | | | | (_) | |_| | ||  __/\__ \
 \__,_| .__/|_| |_|  \___/ \__,_|\__\___||___/
      |_|
*/

//CREATE
/*
___________ _____ ___ _____ _____
/  __ \ ___ \  ___/ _ \_   _|  ___|
| /  \/ |_/ / |__/ /_\ \| | | |__
| |   |    /|  __|  _  || | |  __|
| \__/\ |\ \| |__| | | || | | |___
\____|_| \_\____|_| |_/\_/ \____/
*/

//create comment
app.post('/v1/articles/comments', function(req, res){
// app.post('/v1/articles/comments', passport.authenticate('jwt', { session: false}), function(req, res){
  var token = getToken(req.headers);
  if (token != "") {
    var decoded = jwt.decode(token, config.secret);
    User.findOne({
      email: decoded.email
    }, function(err, user) {
        if (err)   return res.json({success: false, msg: err});

        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {//end if user not found

            if(req.body.comment._article){

              //get the comment as it was just posted and make a mongo object out of it
              var comment = new Comment(req.body.comment);
              //set comment to published so it's visible by default for now
              comment.published = true;
              //set the comment's author id
              comment._author = user._id;

              // save the comment
              comment.save(function (err, comment) {
                if (err) return handleError(err);

                //todo:
                //move this to the schema when you get the pres working
                Article.findOne({ '_id' : req.body.comment._article }, function (err, article) {
                  if (err)
                    return next(err);

                  //count all the comments that belong to this article
                  Comment.count({ published: true }).where('_article').equals(article._id).exec(function(err, count) {
                      if (err)   return res.json({success: false, msg: err});

                      article.commentCount = count;

                      //get the first max 3 comments and push them to the subdocuments (for preview of the comment thread)
                      Comment.find({ published: true }).where('_article').equals(article._id).sort({ _id : -1}).limit(3).exec(function(err, comments) {
                          if (err)   return res.json({success: false, msg: err});

                          console.log("comments: " + comments);
                          console.log("article: " + article);

                          article.comments = comments;
                          article.save();

                          return res.json({comment: comment, article: article });
                      });

                  });

                });
                //end todo:

              });//end save comment
            }//end if article has an article id
        }//end else (user is authenticated)
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }//end else

});//end post to article/comments

//end create comment



//create articles
app.post('/v1/articles', function(req, res){

  var token = getToken(req.headers);

  if (token != "") {
    var decoded = jwt.decode(token, config.secret);

    User.findOne({
      email: decoded.email
    }, function(err, user) {
        if (err)   return res.json({success: false, msg: err});

        if (!user) {
          return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
        } else {//if user is found

            //get the article as it was just posted and make a mongo object out of it
            //console.log(req.body.article)
            var article = new Article(req.body.article);
            //todo: change this to false by default
            article.published = true;
            //set the author to user who owns this token
            article._author = user._id;
            //convert the html body to json format
            article.content = htmlToArticleJson(article.body);
            //convert tags from a comma delimited string to an array
            article.tags = req.body.article.tags ? req.body.article.tags.replace(/ /g,'').split(",") : [];
            console.log(article.tags)

            if(typeof(req.files) != 'undefined'){

              var medias = req.files;

              for (var i = medias.length - 1; i >= 0; i--) {

                var media = new Media(medias[i]);
                //set the author to user who owns this token
                media._author = user._id;

                //save media object
                media.save(function (err, media){
                  if(err) return handleError(err);

                  lwip.open("./uploads/" + media.filename, function(err, image){
                    if (err)   return res.json({success: false, msg: err});
                    var newWidth = 400;
                    //find new height by geting ratio from width
                    var newHeight = ( newWidth / image.width() ) * image.height();
                    //resize image on disk
                    image.resize(newWidth, newHeight, function(err, resImage) {
                      resImage.writeFile("./uploads/" + media.filename, function(err){
                        if (err)   return res.json({success: false, msg: err});

                        console.log("resized: " + media.filename);

                      }); //end write
                    });//end resize
                  });//end open lwip
                });//end save media object
                //add this media reference to the article
                article._medias.push(media._id);

              }//end for medias

            }//end if has medias


            article.save(function (err, article) {
              if (err) return err;

              // //add the newly created article to the search index
              // var opt = {};
              // searchIndex(options, function(err, si) {
              //   //add
              //   si.add(article, opt, function (err) {
              //     if (err) console.log(err);
              //     else
              //       console.log(article.title + " added to search index!");
              //   });

              // });

              return res.json(article);

            });

        }//end else (user is authenticated)
    });
  } else {
    return res.status(403).send({success: false, msg: 'No token provided.'});
  }//end else


});

//READ
/*
______ _____ ___ ______
| ___ \  ___/ _ \|  _  \
| |_/ / |__/ /_\ \ | | |
|    /|  __|  _  | | | |
| |\ \| |__| | | | |/ /
\_| \_\____|_| |_/___/
*/

var validateObjectId = function (id) {

  var ObjectId = mongoose.Types.ObjectId;

  var id = id.trim();

  if( ObjectId.isValid(id) && ObjectId(id) == id ){
    return true;
  }

  return false;
};

//get an articles' n latest comments
app.get('/v1/articles/comments/:id/:count', function(req, res){
  console.log("latest comments");
  //if no count was specified
  var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

  var q = Comment.find({ published: true, _article: req.params.id }).populate('_author').sort({'_id': -1}).limit(count);

  q.exec(function(err, comments) {
      if (err)
        return err;

      return res.json(comments);
  });

});

//get next published article
app.get('/v1/articles/next/:_id', function(req, res){
  if(req.params._id && req.params._id != "undefined"){

    //if the string supplied is an object id
    if(validateObjectId(req.params._id)){
      Article.find({}).where('_id').lt(req.params._id).sort({ _id : -1}).limit(1).populate('_medias _author').exec(function(err, article) {
        if (err) return err;
        res.json(article[0]);
      });
    }else{
      var slug = req.params._id;
      //otherwise try to look it up by slug
      Article.findOne({slug: slug}, function(err, article) {
        //then find it by id
        if (article){
          Article.find({}).where('_id').lt(article._id).sort({ _id : -1}).limit(1).populate('_medias _author').exec(function(err, article) {
            if (err)   return res.json({success: false, msg: err});
            res.json(article[0]);
          });//end find next
        }//end if
        else{
            res.json({message: "no record found"});
        }

      });//end findOne

    }//end else


  }//end if req has id
  else{
    res.json({message: "no id supplied!"});
  }//end else

});

//get previously published article
app.get('/v1/articles/previous/:_id', function(req, res){
  if(req.params._id && req.params._id != "undefined"){

    //if the string supplied is an object id
    if(validateObjectId(req.params._id)){
      Article.find({}).where('_id').gt(req.params._id).sort({ _id : -1}).limit(1).populate('_medias _author').exec(function(err, article) {
        if (err)   return res.json({success: false, msg: err});

        res.json(article[0]);
      });
    }else{
      var slug = req.params._id;
      //otherwise try to look it up by slug
      Article.findOne({slug: slug}, function(err, article) {
        //then find it by id
        if (article){
          Article.find({}).where('_id').gt(article._id).sort({ _id : -1}).limit(1).populate('_medias _author').exec(function(err, article) {
            if (err)   return res.json({success: false, msg: err});

            res.json(article[0]);
          });//end find next
        }//end if
        else{
            res.json({message: "no record found"});
        }

      });//end findOne

    }//end else

  }//end if req has id
  else{
    res.json({message: "no id supplied!"});

  }//end else

});


//get any article, passing a date range is optional
app.get('/v1/articles/any/:range', function(req, res){

  //if the request params have a range defined
  if(req.params.range){
    var range = req.params.range;
    range.replace(/ /g,''); //remove spaces
    range = range.split(','); //turn into array separated by commas
    var start = new Date(range[0]);
    var end = new Date(range[1]);
    //set query between the ranges
    query = {"created_at": {$gte: start, $lt: end}};
    // query = {"created_at": {$gte: start, $lt: end}, published : true };
  }//end if date range
  else{
    //set to query all
    // query = { published : true };
    query = {};
  }//end else


  Article.count(query).exec(function(err, count){
    var random = Math.floor(Math.random() * count);
    Article.findOne().skip(random).populate('_medias _author').exec(
      function (err, result) {
        // result is random
        res.json(result);
    });
  });

});



//get the n latest comments by an author
app.get('/v1/authors/comments/:id/:count', function(req, res){

  //if no count was specified
  var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

  var q = Comment.find({ published: true, _author: req.params.id }).sort({'_id': -1}).limit(count);

  q.exec(function(err, comments) {
      if (err)
        return console.error(err);

      return res.json(comments);
  });

});

//get an author by username
app.get('/v1/authors/:username', function(req, res){

  //if no count was specified
  if(!req.params.username){
    q = {};
  }else{
    q = { username: req.params.username };
  }

  User.findOne(q, function(err, user) {
      if (err)
        return console.error(err);

      return res.json(user);
  });

});


//get all articles by an author
app.get('/v1/articles/authors/:username/:count', function(req, res){
  //if no count was specified
  var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

  //get user by username
  User.findOne({ username: req.params.username}, function (err, user) {
    if (err)
      return next(err);

    Article.find({ published: true, _author: user._id }).sort({'_id': -1}).limit(count).exec(function(err, articles) {
        if (err)
          return console.error(err);

        return res.json(articles);
    });//end fine artcles

  });//end find user by username

});

// //get article by it's id
// app.get('/v1/:_id', function(req, res){
//   //query in Mongoose by slug
//   Article.findOne({ _id: req.params._id}, function (err, article) {
//     if (err)
//       return next(err);
//
//     res.json(article);
//   });
// });

//get n latest articles
app.get('/v1/articles/:count', function(req, res){

  //if no count was specified
  var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;


  var q = Article.find({}).sort({'_id': -1}).limit(count).populate("_medias _author");
  // var q = Article.find({ published: true }).sort({'_id': -1}).limit(count).populate("_medias");

  q.exec(function(err, articles) {
      if (err)
        return console.error(err);

      return res.json(articles);
  });

});

//get article by it's slug
app.get('/v1/:slug', function(req, res){
  //query in Mongoose by slug

  // Article.findOne({ slug: req.params.slug}).exec(function (err, article) {
  Article.findOne({ slug: req.params.slug}).populate('_medias _author').exec(function (err, article) {
    if (err)
      return next(err);

    res.json(article);
  });


});

//search
//related

app.get('/v1/articles/search/:search', function(req, res){

  if(req.params.search){
    searchIndex(options, function(err, si) {

// //USE THIS TO FLUSH THE SEARCH INDEX
//       si.flush(function(err) {
//         if (!err) console.log('success!')
//       });
// //USE THIS TO FLUSH THE SEARCH INDEX
//

      q = {};
     q.query = {'*': [req.params.search]}; //search for string in all ('*') fields
      si.search(q, function (err, searchResults) {
        //do something with searchResults
        return res.json(searchResults);
      });
    });
  }//end if

});

//get n related articles
app.get('/v1/related/:slug/:count', function(req, res){

  var count = parseInt(count) > 0 ? count : 5;

  if(req.params.slug && req.params.slug != "undefined"){

    // //if the string supplied is an object id
    // if(validateObjectId(req.params.slug)){
    //   Article.find({ tags: article.tags}).sort({ _id : -1}).populate('_medias _author').exec(function(err, article) {
    //   // Article.find({ published: true }).sort({ _id : -1}).populate('_medias').exec(function(err, article) {
    //     if (err)   return res.json({success: false, msg: err});
    //
    //     res.json(article[0]);
    //   });
    // }else{
      var slug = req.params.slug;
      //otherwise try to look it up by slug
      Article.findOne({slug: slug}, function(err, article) {
        //then find it by id
        if(article){
          var tags = article.tags;

          console.log(tags);

          Article.find({ tags: { $in : tags }, _id: { $nin: [ article._id] }  }).sort({ _id : -1}).limit(count).populate('_medias _author').exec(function(err, articles) {
            if (err)   return res.json({success: false, msg: err});

            res.json(articles);

          });//end find next
        }//end if

      });//end findOne

    // }//end else



  }//end if req has id
  else{
    res.json({message: "no id supsvsvsplied!"});
  }//end else

});

//UPDATE
/*
_   _____________  ___ _____ _____
| | | | ___ \  _  \/ _ \_   _|  ___|
| | | | |_/ / | | / /_\ \| | | |__
| | | |  __/| | | |  _  || | |  __|
| |_| | |   | |/ /| | | || | | |___
\___/\_|   |___/ \_| |_/\_/ \____/
*/

app.put('/v1/articles/', function(req, res){

    var article = req.body.article;

    var token = getToken(req.headers);

    if (token != "") {
      var decoded = jwt.decode(token, config.secret);

      User.findOne({
        email: decoded.email
      }, function(err, user) {
          if (err)   return res.json({success: false, msg: err});
          //if the _author is an object get it's id, if it's id is undefined and it's just a string, get the string

          if(typeof article._author !== 'undefined'){
            var author_user_id = typeof article._author === 'object' ? article._author._id : article._author;
          }
          console.log(author_user_id);

          if (!user) {
            return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
          } else if ( author_user_id == user._id) {//if user is found and is the author of the article
            //if the user is an editor allow them to publish/un-publish else, make any edits set published
            user.role = 'editor';
            article.published = user.role == 'editor' ? article.published : false;
            //convert the html body to json format
            article.content = htmlToArticleJson(article.body);
            //convert tags from a comma delimited string to an array
            article.tags = req.body.article.tags ? req.body.article.tags.replace(/ /g,'').split(",") : [];
            console.log(article.tags)

              Article.findByIdAndUpdate(article._id, article, function (err, article) {
                if (err)
                  return next(err);

                //delete article from search index then re-add the newly updated version
              //  searchIndex(options, function(err, si) {
                  //delete the article by id from the search index
                  // si.del(req.params.id, function(err) {
                  //   if (!err) console.log(article.title + " removed from index.");
                  // });
                  // var opt = {};
                  // //add the new article back to the index
                  // si.add(article, opt, function (err) {
                  //   if (err) console.log(err);
                  //   else
                  //     console.log(article.title + " added to search index!");
                  // });
              //  });

                return res.json(article);

              });

          }else{//end else (user is authenticated)
            return res.status(403).send({success: false, msg: 'Authentication failed. User doesn\'t have access to this.'});
          }//end else
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }//end else


});

//DELETE
/*
______ _____ _     _____ _____ _____
|  _  \  ___| |   |  ___|_   _|  ___|
| | | | |__ | |   | |__   | | | |__
| | | |  __|| |   |  __|  | | |  __|
| |/ /| |___| |___| |___  | | | |___
|___/ \____/\_____|____/  \_/ \____/
*/

app.delete('/v1/articles/:id', function(req, res){

    var token = getToken(req.headers);

    if (token != "") {
      var decoded = jwt.decode(token, config.secret);

      User.findOne({
        email: decoded.email
      }, function(err, user) {
          if (err)   return res.json({success: false, msg: err});

          if (!user) {
            return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
          } else {//if user is found

              Article.findById(req.params.id, function (err, article) {
                if (err) return next(err);


                if ( String(article._author) == String(user._id)) {//if user is found and is the author of the article

                  var realdeletefromanadmin = true;
                  //this actually deletes models from the db
                  if(realdeletefromanadmin){
                    //if the logged in user has proper permissions
                    //find the article by id and soft delete it
                    Article.findByIdAndRemove(req.params.id, req.body, function (err, article) {
                      if (err) return next(err);
                      //remove the article from the search index
                    /*  searchIndex(options, function(err, si) {
                        si.del(req.params.id, function(err) {
                          if (!err) console.log(article.title + " removed from index.");
                        });
                      });*/

                      res.json(article);

                    });

                  }//end real deletes
                  else{ //do a soft delete that can be restored (like a user changing their mind later)
                    Article.softdelete(function(err, arcl) {
                      if (err) { callback(err); }
                      callback(null, arcl);
                    });
                  }// end else
                }else{//end if
                  return res.status(403).send({success: false, msg: 'Authentication failed. You don\'t have permission to delete this.'});
                }//end if user ids don't match

              });//end findById

          }//end else (user is authenticated)
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }//end else


});


app.delete('/v1/articles/comments/:id', function(req, res){


    var token = getToken(req.headers);

    if (token != "") {
      var decoded = jwt.decode(token, config.secret);

      User.findOne({
        email: decoded.email
      }, function(err, user) {
          if (err)   return res.json({success: false, msg: err});

          if (!user) {
            return res.status(403).send({success: false, msg: 'Authentication failed. User not found.'});
          } else {//if user is found

              var realdeletefromanadmin = true;
              //this actually deletes models from the db
              if(realdeletefromanadmin){
                //if the logged in user has proper permissions
                //find the comment by id and soft delete it
                Comment.findByIdAndRemove(req.params.id, req.body, function (err, comment) {
                  if (err) return next(err);

                  res.json(comment);

                });
              }//end real delete
              else{//do a soft delete that can be restored (like a user changing their mind later)
                Comment.softdelete(function(err, comnt) {
                  if (err) { callback(err); }
                  callback(null, comnt);
                });
              }//end else do softdelete

              // Comment.restore(function(err, comnt) {
              //   if (err) { callback(err); }
              //   callback(null, comnt);
              // });

          }//end else (user is authenticated)
      });
    } else {
      return res.status(403).send({success: false, msg: 'No token provided.'});
    }//end else

});

//END CRUD

// //PUBLISH
// /*
// __________     ___.   .__  .__       .__
// \______   \__ _\_ |__ |  | |__| _____|  |__
//  |     ___/  |  \ __ \|  | |  |/  ___/  |  \
//  |    |   |  |  / \_\ \  |_|  |\___ \|   Y  \
//  |____|   |____/|___  /____/__/____  >___|  /
//                     \/             \/     \/
// */
//
// app.put('/v1/articles/:id/publish', function(req, res){
//
// });
//
// //end publish

/*
 __            _        _
/ _\ ___   ___| | _____| |_ ___
\ \ / _ \ / __| |/ / _ \ __/ __|
_\ \ (_) | (__|   <  __/ |_\__ \
\__/\___/ \___|_|\_\___|\__|___/

  with socket.io
*/


//emit message through sockets when new article is posted

io.on('connection', function(socket){
  console.log('a user connected');
  socket.on('disconnect', function(){
    console.log('user disconnected');
  });

  socket.on('article created', function(artcl){
    io.emit('article created', artcl);
  });

  socket.on('article published', function(artcl){
    io.emit('article published', artcl);
  });

});

//end chat


//listen
http.listen(5000, function(){
  console.log('listening on *:5000');
});
//end listen
