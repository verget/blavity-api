var express = require('express'); //server
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http); //sockets
var fs = require('fs'); //filesystem
var bodyParser = require('body-parser');
var mongoose = require('mongoose'); //mongodb orm
var multer = require('multer'); //file uploads
var request = require('request'); //requests
var morgan = require('morgan'); //logging
var passport = require('passport'); //authentication
var config = require('../config/config'); // get db config file
var jwt = require('jwt-simple'); //json web tokens
var md5 = require('md5');
var slug = require('slug');
var urlencode = require('urlencode');
var PHPUnserialize = require('php-unserialize');
var Q = require('q');
mongoose.Promise = require('q').Promise;
//var urlencode = require('urlencode');
// var oembed      = require('oembed');

// var lwip        = require('lwip');//image processing
var htmlToArticleJson = require('html-to-article-json')({});
var crypto = require('crypto');

//models
var Article = require('../models/Article.js');
var User = require('../models/User.js');
var Media = require('../models/Media.js');
var Comment = require('../models/Comment.js');
var Tag = require('../models/Tag.js');
var Category = require('../models/Category.js');
//end models


//services
var mailService = require('../services/mailService');
//end services
var options = {};

var searchIndex = require('search-index');
//var sindex = searchIndex(options);
var data = require('../indexes/articles.json');


//use ejs as the view engine
app.set('view engine', 'ejs');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Token");
    next();
});


function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, '../uploads/')
    },
    filename: function(req, file, cb) {
        var mimetype = file.mimetype.toLowerCase();
        switch (mimetype) {
            case 'image/jpeg':
                extension = '.jpg';
                break;
            case 'image/gif':
                extension = '.gif';
                break;
            case 'image/png':
                extension = '.png';
                break;
            default:
                extension = '.jpg';
        }
        cb(null, guid() + Date.now() + extension)
            // cb(null, file.fieldname.replace(/\W+/g, '-').toLowerCase() + Date.now() + extension)
    }
});

app.use(multer({ storage: storage }).array('files'));


//connect to mongodb articles database
//mongoose.connect('mongodb://localhost/blog', {  user: 'blavityapi',
//pass: 'sankakukoen',
//db: 'blog'
// authenticationDatabase: 'admin'
//});

mongoose.connect(config.database, { native_parser: true },
    function(err, res) {
        if (err) {
            console.log('ERROR connecting to: ' + config.database + '. ' + err);
        } else {
            console.log('Succeeded connected to: ' + config.database);
        }
    });

var db = mongoose.connection;
// Config body-parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//for serving static pages
/*
 __ _        _   _          ___
/ _\ |_ __ _| |_(_) ___    / _ \__ _  __ _  ___  ___
\ \| __/ _` | __| |/ __|  / /_)/ _` |/ _` |/ _ \/ __|
_\ \ || (_| | |_| | (__  / ___/ (_| | (_| |  __/\__ \
\__/\__\__,_|\__|_|\___| \/    \__,_|\__, |\___||___/
                                     |___/
*/
app.use(express.static('public'));
app.use(express.static('uploads'));
//end static pages


//log to the console
app.use(morgan('dev'));
// Use the passport package in our application
app.use(passport.initialize());


app.get('/', function(req, res) {
    //do nothing
    res.json({ "": "Welcome to blavity API" })
    console.log("Hello");
});


// Read image from folder
app.get('/uploads/:file', function(req, res) {
    file = req.params.file;
    var img = fs.readFileSync(__dirname + "/../uploads/" + file);
    res.writeHead(200, { 'Content-Type': 'image/jpg' });
    res.end(img, 'binary');

});


/*
_    _                               _   _
| |  | |                   /\        | | | |
| |  | |___  ___ _ __     /  \  _   _| |_| |__
| |  | / __|/ _ \ '__|   / /\ \| | | | __| '_ \
| |__| \__ \  __/ |     / ____ \ |_| | |_| | | |
\____/|___/\___|_|    /_/    \_\__,_|\__|_| |_|

*/

//user auth
app.get('/v1/verification/:token', function(req, res) {

    User.findOne({
        user_activation_key: req.params.token
    }, function(err, user) {
        if (err) throw err;
        if (!user) {
            res.send({ success: false, msg: 'Verification failed. User not found.' });
        } else {
            user.user_status = 'active';
            user.save(function(err) {
                if (err) {
                    res.send({ success: false, msg: 'Verification failed.' + err });
                } else {
                    res.send({ success: true, msg: 'Verification success, now you can enter in your account.' });
                }
            });
        }
    });
});

app.post('/v1/send-reset-link', function(req, res) {
    console.log(req.body.email);
    User.findOne({
        email: req.body.email
    }, function(err, user) {
        if (err || !user) {
            res.json({ success: false, msg: 'Reset failed. User not found.' });
        } else {
            user.password_reset_key = crypto.randomBytes(16).toString('hex');
            user.save(function(err) {
                if (err) {
                    res.json({ success: false, msg: 'Reset failed.' + err });
                } else {
                    //send an email with password reset info
                    mailService.sendMail('reset', user._id, function(error, info) {
                        if (error) {
                            res.send({ success: false, msg: "Reset failed. Can't send reset link." });
                        } else {
                            res.send({ success: true, msg: 'Reset link was sent. Check you mail.' });
                        }
                    });
                }
            });
        }
    });
});

app.post('/v1/change-password/', function(req, res) {

    User.findOne({
        password_reset_key: req.body.token
    }, function(err, user) {
        if (err || !user) {
            res.send({ success: false, msg: 'Change failed. User not found.' });
        } else {
            user.password = req.body.password;
            user.save(function(err) {
                if (err) {
                    res.send({ success: false, msg: 'Change failed.' + err });
                } else {
                    res.send({ success: true, msg: 'Changed, now you can enter in your account with new password.' });
                }
            });
        }
    });
});

// pass passport for configuration
require('../services/passport')(passport);


// create a new user account
app.post('/v1/auth/signup', function(req, res) {
    console.log(req.body.user);
    if (!req.body.user.email || !req.body.user.password) {
        console.log('here');
        return res.json({ success: false, msg: 'Please enter email and password.' });
    } else {
        console.log('else');
        req.body.user.wp_password = md5(req.body.user.email);
        req.body.user.user_activation_key = crypto.randomBytes(16).toString('hex');

        var newUser = new User(req.body.user);


        if(newUser.wants_newsletter){
          request.post({
            url:     'https://us3.api.mailchimp.com/2.0/lists/subscribe.json?apikey=2c306f71871447cb0559ad75502ce7aa-us3&id=b53d5e9f25&email[email]='+ newUser.email + '&double_optin=false&send_welcome=true'

          }, function(error, response, body){
            console.log(newUser.email + ' signed up for the newsletter.');

          });
        }//end newsletter signup



        // save the user
        console.log('before save');
        newUser.save(function(err) {
            console.log('saving data ');
            if (err) {
                console.log('error in save');
                return res.json({ success: false, msg: 'User already exists.', error: err });
            } else {
                console.log('save successfully');
                console.log(newUser._id);
                //send an email notification to let the user know they have an account
                mailService.sendMail('verification', newUser._id, function(error, info) {
                    if (error) {
                        return res.json({ success: false, msg: "Can't send verification email.", error: error });
                    } else {
                        return res.json({ success: true, msg: 'Successful created new user.' });
                    }
                });

            }
        });
    }
});


//authenticate user to get token
app.post('/v1/auth/authenticate', function(req, res) {
    User.findOne({
        email: req.body.user.email
    }, function(err, user) {
        if (err) throw err;

        if (!user) {
            res.send({ success: false, msg: 'Authentication failed. User not found.' });
        } else if (user.user_status != 'active') {
            res.send({ success: false, msg: 'Authentication failed. User email not verified.' });
        } else {
            // check if password matches
            user.comparePassword(req.body.user.password, function(err, isMatch) {
                if (isMatch && !err) {
                    // if user is found and password is right create a token
                    var token = jwt.encode(user, config.secret);
                    // return the information including token as JSON
                    res.json({ success: true, token: 'JWT ' + token });
                } else {
                    res.send({ success: false, msg: 'Authentication failed. Wrong password.' });
                }
            });
        }
    });
});


getToken = function(headers) {
    // console.log(headers.cookie)
    // console.log(headers.token)
    console.log("headers: " + JSON.stringify(headers))
    console.log("token: " + headers.token)
    if (headers && headers.token) {
        // if (headers && headers.cookie) {
        var parted = headers.token.split('%20');
        var parted = headers.token.split(' ');
        if (parted.length === 2) {
            return parted[1];
        } else {
            return null;
        }
    } else {
        return null;
    }
};

//end user auth



//create comment
app.post('/v1/getUserInfo', function(req, res) {
    // app.post('/v1/articles/comments', passport.authenticate('jwt', { session: false}), function(req, res){
    var token = getToken(req.headers);
    if (token != "") {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({
            email: decoded.email
        }, function(err, user) {
            if (err) res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });

            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else { //end if user not found
                user.image = "https://www.gravatar.com/avatar/" + md5(user.email.trim().toLowerCase()) + "?d=&s=200";
                console.log(user);
                res.status(200).send({ success: true, user: { "_id": user._id, "email": user.email, "username": user.username, 'image': user.image } });
            } //end else (user is authenticated)
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    } //end else

});

//CRUD ROUTES
/*
             _                   _
  __ _ _ __ (_)  _ __ ___  _   _| |_ ___  ___
 / _` | '_ \| | | '__/ _ \| | | | __/ _ \/ __|
| (_| | |_) | | | | | (_) | |_| | ||  __/\__ \
 \__,_| .__/|_| |_|  \___/ \__,_|\__\___||___/
      |_|
*/

//CREATE
/*
___________ _____ ___ _____ _____
/  __ \ ___ \  ___/ _ \_   _|  ___|
| /  \/ |_/ / |__/ /_\ \| | | |__
| |   |    /|  __|  _  || | |  __|
| \__/\ |\ \| |__| | | || | | |___
\____|_| \_\____|_| |_/\_/ \____/
*/

//create comment
app.post('/v1/articles/comments', function(req, res) {
    // app.post('/v1/articles/comments', passport.authenticate('jwt', { session: false}), function(req, res){
    var token = getToken(req.headers);
    if (token != "") {
        var decoded = jwt.decode(token, config.secret);
        User.findOne({
            email: decoded.email
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else { //end if user not found

                if (req.body.comment._article) {

                    //get the comment as it was just posted and make a mongo object out of it
                    var comment = new Comment(req.body.comment);
                    //set comment to published so it's visible by default for now
                    comment.published = true;
                    //set the comment's author id
                    comment._author = user._id;

                    // save the comment
                    comment.save(function(err, comment) {
                        if (err) return handleError(err);

                        //todo:
                        //move this to the schema when you get the pres working
                        Article.findOne({ '_id': req.body.comment._article }, function(err, article) {
                            if (err)
                                return next(err);

                            //count all the comments that belong to this article
                            Comment.count({ post_status: 'publish' }).where('_article').equals(article._id).exec(function(err, count) {
                                if (err) throw err;

                                article.commentCount = count;

                                //get the first max 3 comments and push them to the subdocuments (for preview of the comment thread)
                                Comment.find({ post_status: 'publish' }).where('_article').equals(article._id).sort({ _id: -1 }).limit(3).exec(function(err, comments) {
                                    if (err) throw err;

                                    console.log("comments: " + comments);
                                    console.log("article: " + article);

                                    article.comments = comments;
                                    article.save();

                                    return res.json({ comment: comment, article: article });
                                });

                            });

                        });
                        //end todo:

                    }); //end save comment
                } //end if article has an article id
            } //end else (user is authenticated)
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    } //end else

}); //end post to article/comments

//end create comment



//create articles
app.post('/v1/articles', function(req, res) {

    var token = getToken(req.headers);

    if (token != "") {
        var decoded = jwt.decode(token, config.secret);

        User.findOne({
            email: decoded.email
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else { //if user is found

                //get the article as it was just posted and make a mongo object out of it
                var article = new Article(JSON.parse(req.body.article));
                //todo: change this to false by default
                //article.published = true;
                //set the author to user who owns this token
                article._author = user._id;
                //convert the html body to json format
                article.content = htmlToArticleJson(article.body);

                ///tags
                /*  var tags = JSON.parse(req.body.article).tags; //gets tags as a comma delimited string
                  //tags = tags.split(","); // makes tags an array
                  //loop through this tags array
                  console.log(tags);
                  for (var i = tags.length - 1; i >= 0; i--) {
                    //find out if tag(s) exists in tags
                    var tg = tags[i].toLowerCase(); //make all tags lowercase
                    tg = tg.trim(); //remove empty space before or after tag
                    Tag.findOne({ slug: tags[i] }).exec(function (err, tag) {
                      if (err)
                        return next(err);

                      var tag = tag ? tag : new Tag({ text: tags[i] });//if tag doesnnot exist, create it

                      article.tags.push(tag._id); //push the id to article array
                    });

                  }//end for tags

                  ///categories
                  var categories = JSON.parse(req.body.article).categories; //gets tags as a comma delimited string
                  //categories = categories.split(","); // makes tags an array
                  //loop through this tags array
                  for (var i = categories.length - 1; i >= 0; i--) {
                    //find out if Category(s) exists in Category
                    var ct = categories[i].toLowerCase(); //make all Category lowercase
                    ct = ct.trim(); //remove empty space before or after Category
                    Category.findOne({ slug: categories[i] }).exec(function (err, category) {
                      if (err)
                        return next(err);

                      var category = category ? category : new Category({ text: categories[i] });//if Category doesnnot exist, create it

                      article.categories.push(category._id); //push the id to article array
                    });

                  }//end for categories*/

                if (typeof(req.files) != 'undefined') {

                    var medias = req.files;

                    for (var i = medias.length - 1; i >= 0; i--) {

                        var media = new Media(medias[i]);
                        //set the author to user who owns this token
                        media._author = user._id;

                        //save media object
                        media.save(function(err, media) {
                            if (err) return handleError(err);

                            // lwip.open("../uploads/" + media.filename, function(err, image){
                            //   if (err) throw err;
                            //   var newWidth = 400;
                            //   //find new height by geting ratio from width
                            //   var newHeight = ( newWidth / image.width() ) * image.height();
                            //   //resize image on disk
                            //   image.resize(newWidth, newHeight, function(err, resImage) {
                            //     resImage.writeFile("../uploads/" + media.filename, function(err){
                            //       if (err) throw err;
                            //
                            //       console.log("resized: " + media.filename);
                            //
                            //     }); //end write
                            //   });//end resize
                            // });//end open lwip
                        }); //end save media object
                        //add this media reference to the article
                        article._medias.push(media._id);

                    } //end for medias

                } //end if has medias

                console.log(article)
                article.save(function(err, article) {
                    if (err) return res.json(err);

                    // //add the newly created article to the search index
                    // var opt = {};
                    // searchIndex(options, function(err, si) {
                    //   //add
                    //   si.add(article, opt, function (err) {
                    //     if (err) console.log(err);
                    //     else
                    //       console.log(article.title + " added to search index!");
                    //   });

                    // });

                    return res.json(article);

                });

            } //end else (user is authenticated)
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    } //end else


});

//READ
/*
______ _____ ___ ______
| ___ \  ___/ _ \|  _  \
| |_/ / |__/ /_\ \ | | |
|    /|  __|  _  | | | |
| |\ \| |__| | | | |/ /
\_| \_\____|_| |_/___/
*/

var validateObjectId = function(id) {

    var ObjectId = mongoose.Types.ObjectId;

    var id = id.trim();

    if (ObjectId.isValid(id) && ObjectId(id) == id) {
        return true;
    }

    return false;
};

// //oembed
// //get an embed object from an embed link using the oembed library
// app.get('/v1/oembed/:uri', function(req, res){
//
//   oembed.fetch('https://www.youtube.com/watch?v=hfJvnb4H3TE', { maxwidth: 1920 }, function(error, result) {
//       if (error)
//           console.error(error);
//       else
//           console.log("oEmbed result", result);
//   });
//
// });
//


//get an articles' n latest comments
app.get('/v1/articles/comments/:id/:count', function(req, res) {
    console.log("latest comments");
    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

    var q = Comment.find({ post_status: 'publish', _article: req.params.id }).populate('_author').sort({ '_id': -1 }).limit(count);

    q.exec(function(err, comments) {
        if (err)
            return console.error(err);

        return res.json(comments);
    });

});

//get next published article
app.get('/v1/articles/next/:_id', function(req, res) {
    if (req.params._id && req.params._id != "undefined") {

        //if the string supplied is an object id
        if (validateObjectId(req.params._id)) {
            Article.find({ post_status: 'publish' }).where('_id').lt(req.params._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {
                if (err) throw err;

                res.json(article[0]);
            });
        } else {
            var slug = req.params._id;
            //otherwise try to look it up by slug
            Article.findOne({ slug: slug }, function(err, article) {
                //then find it by id
                if (article) {
                    Article.find({ post_status: 'publish' }).where('_id').lt(article._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {
                        if (err) throw err;

                        res.json(article[0]);
                    }); //end find next
                } //end if

            }); //end findOne

        } //end else


    } //end if req has id
    else {
        res.json({ message: "no id supplied!" });
    } //end else

});

//get next published article in a category
app.get('/v1/articles/next/:_id/category/:category', function(req, res) {
    if (req.params._id && req.params._id != "undefined") {
        //get the category from the get param
        var category = req.params.category ? req.params.category : "news";

        //find the specified category by slug
        Category.find({ slug: category }).exec(function(err, categories) {

            //if the string supplied is an object id
            if (validateObjectId(req.params._id)) {

                //find the next article filtered by category
                Article.find({ post_status: 'publish', categories: categories[0]._id }).where('_id').lt(req.params._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {
                    if (err) throw err;

                    res.json(article[0]);
                }); //end find article

            } else {
                var slug = req.params._id;
                //otherwise try to look it up by slug to get it's id
                Article.findOne({ slug: slug }, function(err, article) {
                    //then find it by id
                    if (article) {
                        //find the specified category by slug
                        //find the next article filtered by category
                        Article.find({ post_status: 'publish' }).where('_id').lt(article._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {
                            if (err) throw err;

                            res.json(article[0]);
                        }); //end find next article

                    } //end if

                }); //end findOne

            } //end else

        }); //end find category


    } //end if req has id
    else {
        res.json({ message: "no id supplied!" });
    } //end else

});


//get next published video article
app.get('/v1/articles/next/:_id/type/:type', function(req, res) {
    if (req.params._id && req.params._id != "undefined") {
        var type = req.params.type ? req.params.type : "default";

        //if the string supplied is an object id
        if (validateObjectId(req.params._id)) {

            //find the next article filtered by category
            Article.find({ post_status: 'publish', postType: type }).where('_id').lt(req.params._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {
                if (err) throw err;

                res.json(article[0]);
            }); //end find article

        } else {
            var slug = req.params._id;
            //otherwise try to look it up by slug to get it's id
            Article.findOne({ slug: slug }, function(err, article) {
                //then find it by id
                if (article) {
                    //find the specified category by slug
                    //find the next article filtered by category
                    Article.find({ post_status: 'publish', postType: type }).where('_id').lt(article._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {
                        if (err) throw err;

                        res.json(article[0]);
                    }); //end find next article

                } //end if

            }); //end findOne

        } //end else

    } //end if req has id
    else {
        res.json({ message: "no id supplied!" });
    } //end else

});


//get previously published article
app.get('/v1/articles/previous/:_id', function(req, res) {
    if (req.params._id && req.params._id != "undefined") {

        Article.find({ post_status: 'publish' }).where('_id').gt(req.params._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {

            if (err) throw err;

            res.json(article[0]);
        });
    } //end if req has id
    else {
        res.json({ message: "no id supplied!" });

    } //end else

});

//get previously published article in a category
app.get('/v1/articles/previous/:_id/category/:category', function(req, res) {
    if (req.params._id && req.params._id != "undefined") {
        //get the category from the get param
        var category = req.params.category ? req.params.category : "*";
        //find the category by slug
        Category.find({ slug: category }).exec(function(err, categories) {

            Article.find({ post_status: 'publish' }).where('_id').gt(req.params._id).sort({ _id: -1 }).limit(1).populate('_medias _author tags categories').exec(function(err, article) {

                if (err) throw err;

                res.json(article[0]);
            });

        }); //end find category

    } //end if req has id
    else {
        res.json({ message: "no id supplied!" });

    } //end else

});


//get any article, passing a date range is optional
app.get('/v1/articles/any/:range', function(req, res) {

    //if the request params have a range defined
    if (req.params.range) {
        var range = req.params.range;
        range.replace(/ /g, ''); //remove spaces
        range = range.split(','); //turn into array separated by commas
        var start = new Date(range[0]);
        var end = new Date(range[1]);
        //set query between the ranges
        query = { "created_at": { $gte: start, $lt: end } };
        // query = {"created_at": {$gte: start, $lt: end}, published : true };
    } //end if date range
    else {
        //set to query all
        query = { post_status: 'publish' };
        // query = {};
    } //end else


    Article.count(query).exec(function(err, count) {
        var random = Math.floor(Math.random() * count);
        Article.findOne().skip(random).populate('_medias _author tags categories').exec(
            function(err, result) {
                // result is random
                res.json(result);
            });
    });

});



//get the n latest comments by an author
app.get('/v1/authors/comments/:id/:count', function(req, res) {

    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

    var q = Comment.find({ post_status: 'publish', _author: req.params.id }).sort({ '_id': -1 }).limit(count);

    q.exec(function(err, comments) {
        if (err)
            return console.error(err);

        return res.json(comments);
    });

});

//get an author by username
app.get('/v1/authors/:username', function(req, res) {

    if (!req.params.username) {
        q = {};
    } else {
        q = { username: req.params.username };
    }

    User.findOne(q, function(err, user) {
        if (err)
            return console.error(err);

        return res.json(user);
    });

});


//get all articles by an author
app.get('/v1/articles/authors/:username/:count/:offset', function(req, res) {
    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
    var offset = req.params.offset && parseInt(req.params.offset) != 0 ? Math.abs(req.params.offset) : 0;

    //get user by username
    User.findOne({ username: req.params.username }, function(err, user) {
        if (err)
            return next(err);

        Article.find({ post_status: 'publish', _author: user._id }).sort({ '_id': -1 }).skip(offset).limit(count).populate('_medias _author tags categories').exec(function(err, articles) {
            if (err)
                return console.error(err);

            return res.json(articles);
        }); //end fine artcles

    }); //end find user by username

});

//get all articles by an author
app.get('/v1/articles/authorsWillAllCount/:username/:count', function(req, res) {
    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

    //get user by username
    User.findOne({ username: req.params.username }, function(err, user) {
        if (err)
            return next(err);

        Article.find({ post_status: 'publish', _author: user._id }).sort({ '_id': -1 }).limit(count).populate('_medias _author tags categories').exec(function(err, articles) {
            if (err)
                return console.error(err);
            Article.count({ _author: user._id }, function(err, count) {
                return res.json({ articles: articles, totalArticle: count });
            })

        }); //end fine artcles

    }); //end find user by username

});

//get all articles by an author with post type, limit and offset
app.get('/v1/articles/authorposts/:username/:post_status/:count/:offset', function(req, res) {
    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) !== 0 ? Math.abs(req.params.count) : 10;
    var offset = req.params.offset && parseInt(req.params.offset) !== 0 ? Math.abs(req.params.offset) : 0;
    var post_status = req.params.post_status && req.params.post_status !== "" ? req.params.post_status : "publish";

    //get user by username
    User.findOne({ username: req.params.username }, function(err, user) {
        if (err)
            return next(err);

        Article.find({ post_status: post_status, _author: user._id }).sort({ '_id': -1 }).skip(offset).limit(count).populate('_medias _author tags categories').exec(function(err, articles) {
            if (err)
                return console.error(err);
            Article.count({ _author: user._id }, function(err, count) {
                return res.json({ articles: articles, totalArticle: count });
            })

        }); //end fine artcles

    }); //end find user by username

});

// //get article by it's id
// app.get('/v1/:_id', function(req, res){
//   //query in Mongoose by slug
//   Article.findOne({ _id: req.params._id}, function (err, article) {
//     if (err)
//       return next(err);
//
//     res.json(article);
//   });
// });

//get articles by tags
app.get('/v1/articles/tags/:tags/:count', function(req, res) {
    //get the tags
    var tags = req.params.tags.split(",");
    //remove the white spaces
    for (var i = 0; i < tags.length; i++) {
        tags[i] = tags[i].trim();
        tags[i] = tags[i].toLowerCase();
    }

    var tag_ids = [];

    //find the specified tags by name
    Tag.find({ slug: { $in: tags } }).exec(function(err, tags) {
        //push tag ids to an array
        for (var i = 0; i < tags.length; i++) {
            tag_id = String(tags[i]._id);
            tag_ids.push(tag_id);
        }

        //if no count was specified
        var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

        var q = Article.find({ tags: { $in: tag_ids } }).sort({ '_id': -1 }).limit(count).populate("_medias _author tags categories");

        q.exec(function(err, articles) {
            if (err)
                return console.error(err);

            return res.json(articles);
        });

    });


});


//get articles by tags
app.get('/v1/articles/tags/:tags/:count/:offset', function(req, res) {
    //get the tags
    var tags = req.params.tags.split(",");
    //remove the white spaces
    for (var i = 0; i < tags.length; i++) {
        tags[i] = tags[i].trim();
        tags[i] = tags[i].toLowerCase();
    }

    var tag_ids = [];

    //find the specified tags by name
    Tag.find({ slug: { $in: tags } }).exec(function(err, tags) {
        //push tag ids to an array
        for (var i = 0; i < tags.length; i++) {
            tag_id = String(tags[i]._id);
            tag_ids.push(tag_id);
        }

        //if no count was specified
        var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
        //the offset
        var offset = parseInt(req.params.offset);

        var q = Article.find({ tags: { $in: tag_ids } }).sort({ '_id': -1 }).skip(offset).limit(count).populate("_medias _author tags categories");

        q.exec(function(err, articles) {
            if (err)
                return console.error(err);

            return res.json(articles);
        });

    });


});





//get articles by categories
app.get('/v1/articles/categories/:categories/:count', function(req, res) {
    //get the categories
    var categories = req.params.categories.split(",");
    //remove the white spaces
    for (var i = 0; i < categories.length; i++) {
        categories[i] = categories[i].trim();
        categories[i] = categories[i].toLowerCase();
    }

    var category_ids = [];

    //find the specified categories by name
    Category.find({ slug: { $in: categories } }).exec(function(err, categories) {
        //push tag ids to an array
        for (var i = 0; i < categories.length; i++) {
            category_id = String(categories[i]._id);
            category_ids.push(category_id);
        }

        //if no count was specified
        var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;

        var q = Article.find({ post_status: 'publish', categories: { $in: category_ids } }).sort({ '_id': -1 }).limit(count).populate("_medias _author tags categories");

        q.exec(function(err, articles) {
            if (err)
                return console.error(err);

            return res.json(articles);
        });

    });


});


//get articles by categories
app.get('/v1/articles/categories/:categories/:count/:offset', function(req, res) {
    //get the categories
    var categories = req.params.categories.split(",");
    //remove the white spaces
    for (var i = 0; i < categories.length; i++) {
        categories[i] = categories[i].trim();
        categories[i] = categories[i].toLowerCase();
    }

    var category_ids = [];

    //find the specified categories by name
    Category.find({ slug: { $in: categories } }).exec(function(err, categories) {
        //push tag ids to an array
        for (var i = 0; i < categories.length; i++) {
            category_id = String(categories[i]._id);
            category_ids.push(category_id);
        }

        //if no count was specified
        var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
        //the offset
        var offset = parseInt(req.params.offset);

        var q = Article.find({ post_status: 'publish', categories: { $in: category_ids } }).sort({ '_id': -1 }).skip(offset).limit(count).populate("_medias _author tags categories");

        q.exec(function(err, articles) {
            if (err)
                return console.error(err);

            return res.json(articles);
        });

    });


});


//get n latest articles by type
app.get('/v1/articles/type/:type/:count', function(req, res) {

    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
    //the type
    var type = req.params.type.trim();

    var q = Article.find({ post_status: 'publish', postType: type }).sort({ '_id': -1 }).limit(count).populate("_medias _author tags categories");

    q.exec(function(err, articles) {
        if (err)
            return console.error(err);

        return res.json(articles);
    });

});

//get n latest articles by type
app.get('/v1/articles/type/:type/:count/:offset', function(req, res) {

    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
    //the offset
    var offset = parseInt(req.params.offset);
    //the type
    var type = req.params.type.trim();

    var q = Article.find({ post_status: 'publish', postType: type }).sort({ '_id': -1 }).skip(offset).limit(count).populate("_medias _author tags categories");

    q.exec(function(err, articles) {
        if (err)
            return console.error(err);

        return res.json(articles);
    });

});


//get n latest articles
app.get('/v1/articles/:count', function(req, res) {

    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
    //the offset
    var q = Article.find({ post_status: 'publish' }).sort({ '_id': -1 }).limit(count).populate("_medias _author tags categories");

    q.exec(function(err, articles) {
        if (err)
            return console.error(err);

        return res.json(articles);
    });

});

app.get('/v1/articles/:count/:offset', function(req, res) {

    //if no count was specified
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 10;
    //the offset
    var offset = parseInt(req.params.offset);

    var q = Article.find({ post_status: 'publish' }).sort({ '_id': -1 }).skip(offset).limit(count).populate("_medias _author tags categories");

    q.exec(function(err, articles) {
        if (err)
            return console.error(err);

        return res.json(articles);
    });

});

/*Get Category*/
app.get('/v1/category/:count', function(req, res) {

    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 100;
    //the offset
    var q = Category.find({}).limit(count).sort({ '_id': -1 });
    //var q = Category.find({}).limit(count).sort({'_id': -1});

    q.exec(function(err, category) {
        if (err)
            return console.error(err);

        return res.json(category);
    });

});

/*Get Like Category*/
app.get('/v1/categorylike/:slug', function(req, res) {

    //  var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 100;
    //the offset
    var q = Category.find({ name: new RegExp('^' + req.params.slug + '$', "i") }).sort({ '_id': -1 });

    q.exec(function(err, category) {
        if (err)
            return console.error(err);

        return res.json(category);
    });

});


/*Get Tags*/
app.get('/v1/tags/:count', function(req, res) {

    //the offset
    var count = req.params.count && parseInt(req.params.count) != 0 ? Math.abs(req.params.count) : 100; // var q = Article.find({post_status:'publish'}).sort({'_id': -1}).limit(count).populate("_medias");
    var q = Tag.find({}).limit(count).sort({ '_id': -1 });

    q.exec(function(err, tag) {
        if (err)
            return console.error(err);

        return res.json(tag);
    });

});


/*Get Tags*/
app.get('/v1/tagslike/:slug', function(req, res) {

    //the offset
    var q = Tag.find({ name: new RegExp('^' + req.params.slug + '$', "i") }).sort({ '_id': -1 });

    q.exec(function(err, tag) {
        if (err)
            return console.error(err);

        return res.json(tag);
    });

});


//get article by it's slug
app.get('/v1/:slug', function(req, res) {
    //query in Mongoose by slug

    // Article.findOne({ slug: req.params.slug}).exec(function (err, article) {
    Article.findOne({ post_status: 'publish', slug: req.params.slug }).populate('_medias _author tags categories').exec(function(err, article) {
        if (err)
            return next(err);

        res.json(article);
    });


});

/*get video post by tag*/
app.get('/v1/articlebyvideo/:limit/:offset/:number', function(req, res) {
    var limit = req.params.limit && parseInt(req.params.limit) != 0 ? Math.abs(req.params.limit) : 100;
    var offset = req.params.offset && parseInt(req.params.offset) != 0 ? Math.abs(req.params.offset) : 0;
    var number = req.params.number && parseInt(req.params.number) != 0 ? Math.abs(req.params.number) : 3;
    var dbObj = db.db;
    dbObj.eval('db.loadServerScripts();', function(err, result) {
        dbObj.eval("getVideoPostByTag(" + limit + "," + offset + "," + number + ");", function(err, result) {
            res.json({ "results": result });

        })
    });
})


app.get('/fullsearch/:query', function(req, res) {
        var q = {};
        q.query = {
            AND: [{ '*': [req.params.query] }] //search for string 'usa' in all ('*') fields
        }
        searchIndex(options, function(err, si) {
            si.search(q, function(err, searchResults) {
                if (err) return res.json({ "error": err })
                else return res.json(searchResults)
            })
        })

    })
    //search
    //related

app.get('/reindex/blavity', function(req, res) {
    //USE THIS TO FLUSH THE SEARCH INDEX
    searchIndex(options, function(err, si) {
        var l = 0;
        Article.find({ post_status: 'publish' }).sort({ _id: -1 }).skip(0).limit(1000).populate("_medias _author tags categories").exec(function(err, articles) {
            if (err)
                return console.error(err);
            l = articles.length;
            var ctn = 0;
            for (var i = 0; i < articles.length; i++) {
                var opt = {};
                // articles[i].id=articles[i]._id;
                si.add(JSON.parse(JSON.stringify(articles[i])), {}, function(err) {
                    ctn++;
                    console.log(ctn)
                    if (ctn == l) {
                        res.json({ "sucess": l })
                    }
                    if (err) return console.error(err);
                    else console.log('success!')


                }); //end add
            } //end for

        }); //end find all articles

    });
    //USE THIS TO FLUSH THE SEARCH INDEX
});
app.get('/reindex/blavity/:limit/:offset', function(req, res) {
    var offset = req.params.offset && parseInt(req.params.offset) != 0 ? Math.abs(req.params.offset) : 0;
    var pageSize = req.params.limit && parseInt(req.params.limit) != 0 ? Math.abs(req.params.limit) : 10;
    //USE THIS TO FLUSH THE SEARCH INDEX
    searchIndex(options, function(err, si) {
        var l = 0;
        Article.find({ post_status: 'publish' }).sort({ _id: -1 }).skip(offset).limit(pageSize).populate("_medias _author tags categories").exec(function(err, articles) {
            if (err)
                return console.error(err);
            l = articles.length;
            var ctn = 0;
            for (var i = 0; i < articles.length; i++) {
                var opt = {};
                // articles[i].id=articles[i]._id;
                si.add(JSON.parse(JSON.stringify(articles[i])), {}, function(err) {
                    ctn++;
                    console.log(ctn)
                    if (ctn == l) {
                        res.json({ "sucess": l })
                    }
                    if (err) return console.error(err);
                    else console.log('success!')


                }); //end add
            } //end for

        }); //end find all articles

    });
    //USE THIS TO FLUSH THE SEARCH INDEX
});

app.get('/v1/articles/searchPost/:search/:offset/:limit', function(req, res) {
    if (req.params.search) {
        searchIndex(options, function(err, si) {

            // //USE THIS TO FLUSH THE SEARCH INDEX
            //       si.flush(function(err) {
            //         if (!err) console.log('success!')
            //       });
            // //USE THIS TO FLUSH THE SEARCH INDEX
            //

            q = {};
            q.query = { AND: { '*': [req.params.search] }, SORT: { '_id': -1 } }; //search for string in all ('*') fields

            /* q.query = {
                     OR: [{ 'slug': [req.params.search] },
                             { 'subTitle': [req.params.search] },
                             { 'title': [req.params.search] },
                             { 'tags': [req.params.search] },
                             { 'categories': [req.params.search] }
                         ] //search for string 'usa' in all ('*') fields
                 }*/
            q.offset = req.params.offset && parseInt(req.params.offset) != 0 ? Math.abs(req.params.offset) : 0;
            q.pageSize = req.params.limit && parseInt(req.params.limit) != 0 ? Math.abs(req.params.limit) : 2;
            si.search(q, function(err, searchResults) {
                //do something with searchResults
                return res.json(searchResults);
            });
        });
    } //end if

});

//get n related articles
app.get('/v1/related/:slug/:count', function(req, res) {

    var count = parseInt(count) > 0 ? count : 5;

    if (req.params.slug && req.params.slug != "undefined") {

        // //if the string supplied is an object id
        // if(validateObjectId(req.params.slug)){
        //   Article.find({ tags: article.tags}).sort({ _id : -1}).populate('_medias _author tags categories').exec(function(err, article) {
        //   // Article.find({post_status:'publish'}).sort({ _id : -1}).populate('_medias').exec(function(err, article) {
        //     if (err) throw err;
        //
        //     res.json(article[0]);
        //   });
        // }else{
        var slug = req.params.slug;
        //otherwise try to look it up by slug
        Article.findOne({ slug: slug }, function(err, article) {
            //then find it by id
            if (article) {
                var tags = article.tags;

                console.log(tags);

                Article.find({ post_status: 'publish', tags: { $in: tags }, _id: { $nin: [article._id] } }).sort({ _id: -1 }).limit(count).populate('_medias _author tags categories').exec(function(err, articles) {
                    if (err) throw err;

                    res.json(articles);

                }); //end find next
            } //end if

        }); //end findOne

        // }//end else



    } //end if req has id
    else {
        res.json({ message: "no id supplied!" });
    } //end else

});

//UPDATE
/*
_   _____________  ___ _____ _____
| | | | ___ \  _  \/ _ \_   _|  ___|
| | | | |_/ / | | / /_\ \| | | |__
| | | |  __/| | | |  _  || | |  __|
| |_| | |   | |/ /| | | || | | |___
\___/\_|   |___/ \_| |_/\_/ \____/
*/

app.put('/v1/articles/', function(req, res) {

    var article = req.body.article;

    var token = getToken(req.headers);

    if (token != "") {
        var decoded = jwt.decode(token, config.secret);

        User.findOne({
            email: decoded.email
        }, function(err, user) {
            if (err) throw err;
            //if the _author is an object get it's id, if it's id is undefined and it's just a string, get the string

            if (typeof article._author !== 'undefined') {
                var author_user_id = typeof article._author === 'object' ? article._author._id : article._author;
            }


            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else if (author_user_id == user._id) { //if user is found and is the author of the article
                //if the user is an editor allow them to publish/un-publish else, make any edits set published
                user.role = 'editor';
                article.published = user.role == 'editor' ? article.published : false;
                //convert the html body to json format
                article.content = htmlToArticleJson(article.body);
                ///tags
                var tags = req.body.article.tags; //gets tags as a comma delimited string
                tags = tags.split(","); // makes tags an array
                //loop through this tags array
                for (var i = tags.length - 1; i >= 0; i--) {
                    //find out if tag(s) exists in tags
                    var tg = tags[i].toLowerCase(); //make all tags lowercase
                    tg = tg.trim(); //remove empty space before or after tag
                    Tag.findOne({ text: tags[i] }).exec(function(err, tag) {
                        if (err)
                            return next(err);

                        var tag = tag ? tag : new Tag({ text: tags[i] }); //if tag doesnnot exist, create it

                        article.tags.push(tag._id); //push the id to article array
                    });

                } //end for tags

                ///categories
                var categories = req.body.article.categories; //gets tags as a comma delimited string
                categories = categories.split(","); // makes tags an array
                //loop through this tags array
                for (var i = categories.length - 1; i >= 0; i--) {
                    //find out if Category(s) exists in Category
                    var ct = categories[i].toLowerCase(); //make all Category lowercase
                    ct = ct.trim(); //remove empty space before or after Category
                    Category.findOne({ text: categories[i] }).exec(function(err, category) {
                        if (err)
                            return next(err);

                        var category = category ? category : new Category({ text: categories[i] }); //if Category doesnnot exist, create it

                        article.categories.push(category._id); //push the id to article array
                    });

                } //end for categories

                Article.findByIdAndUpdate(article._id, article, function(err, article) {
                    if (err)
                        return next(err);

                    //delete article from search index then re-add the newly updated version
                    searchIndex(options, function(err, si) {
                        //delete the article by id from the search index
                        // si.del(req.params.id, function(err) {
                        //   if (!err) console.log(article.title + " removed from index.");
                        // });
                        // var opt = {};
                        // //add the new article back to the index
                        // si.add(article, opt, function (err) {
                        //   if (err) console.log(err);
                        //   else
                        //     console.log(article.title + " added to search index!");
                        // });
                    });

                    return res.json(article);

                });

            } else { //end else (user is authenticated)
                return res.status(403).send({ success: false, msg: 'Authentication failed. User doesn\'t have access to this.' });
            } //end else
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    } //end else


});

//DELETE
/*
______ _____ _     _____ _____ _____
|  _  \  ___| |   |  ___|_   _|  ___|
| | | | |__ | |   | |__   | | | |__
| | | |  __|| |   |  __|  | | |  __|
| |/ /| |___| |___| |___  | | | |___
|___/ \____/\_____|____/  \_/ \____/
*/

app.delete('/v1/articles/:id', function(req, res) {

    var token = getToken(req.headers);

    if (token != "") {
        var decoded = jwt.decode(token, config.secret);

        User.findOne({
            email: decoded.email
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else { //if user is found

                Article.findById(req.params.id, function(err, article) {
                    if (err) return next(err);


                    if (String(article._author) == String(user._id)) { //if user is found and is the author of the article

                        var realdeletefromanadmin = true;
                        //this actually deletes models from the db
                        if (realdeletefromanadmin) {
                            //if the logged in user has proper permissions
                            //find the article by id and soft delete it
                            Article.findByIdAndRemove(req.params.id, req.body, function(err, article) {
                                if (err) return next(err);
                                //remove the article from the search index
                                searchIndex(options, function(err, si) {
                                    si.del(req.params.id, function(err) {
                                        if (!err) console.log(article.title + " removed from index.");
                                    });
                                });

                                res.json(article);

                            });

                        } //end real deletes
                        else { //do a soft delete that can be restored (like a user changing their mind later)
                            Article.softdelete(function(err, arcl) {
                                if (err) { callback(err); }
                                callback(null, arcl);
                            });
                        } // end else
                    } else { //end if
                        return res.status(403).send({ success: false, msg: 'Authentication failed. You don\'t have permission to delete this.' });
                    } //end if user ids don't match

                }); //end findById

            } //end else (user is authenticated)
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    } //end else


});


app.delete('/v1/articles/comments/:id', function(req, res) {


    var token = getToken(req.headers);

    if (token != "") {
        var decoded = jwt.decode(token, config.secret);

        User.findOne({
            email: decoded.email
        }, function(err, user) {
            if (err) throw err;

            if (!user) {
                return res.status(403).send({ success: false, msg: 'Authentication failed. User not found.' });
            } else { //if user is found

                var realdeletefromanadmin = true;
                //this actually deletes models from the db
                if (realdeletefromanadmin) {
                    //if the logged in user has proper permissions
                    //find the comment by id and soft delete it
                    Comment.findByIdAndRemove(req.params.id, req.body, function(err, comment) {
                        if (err) return next(err);

                        res.json(article);

                    });
                } //end real delete
                else { //do a soft delete that can be restored (like a user changing their mind later)
                    Comment.softdelete(function(err, comnt) {
                        if (err) { callback(err); }
                        callback(null, comnt);
                    });
                } //end else do softdelete

                // Comment.restore(function(err, comnt) {
                //   if (err) { callback(err); }
                //   callback(null, comnt);
                // });

            } //end else (user is authenticated)
        });
    } else {
        return res.status(403).send({ success: false, msg: 'No token provided.' });
    } //end else

});

//END CRUD



// Start Wordpress Sync
app.post('/wpsync', function(req, res) {
    var postData = req.body;
    var keys = Object.keys(postData);
    var postMeta, category, tag, featureImages, user, featurevideo = [];
    var tagsId = [],
        author_id,
        postFeatureImages = null,
        postFeaturevideo = null,
        categoryId = [];
    //console.log(keys);
    if (postData.post) {
        //console.log(JSON.parse(postData.featurevideo));
        var posts = JSON.parse(postData.post);
        if (postData.postMeta) {
            postMeta = JSON.parse(postData.postMeta);
        }

        if (postData.featureImages) {
            featureImages = JSON.parse(postData.featureImages);
            if (featureImages.length) {
                postFeatureImages = featureImages[0];
            }
        }
        if (postData.featurevideo) {
            featurevideo = JSON.parse(postData.featurevideo);
            if (featurevideo.td_last_video) {
                console.log(featurevideo.td_last_video)
                postFeaturevideo = featurevideo.td_video;
            }
        }
        if (postData.category) {
            var c = 0,
                category = JSON.parse(postData.category);
            if (category.length <= 0) {
                impTag();
            }
            for (i = 0; i < category.length; i++) {
                CategorySchemaData = {
                    term_id: category[i].term_id,
                    name: category[i].name,
                    slug: slug(category[i].slug),
                    taxonomy: category[i].taxonomy,
                    description: category[i].description,
                }
                Category.findOneAndUpdate({ term_id: category[i].term_id }, CategorySchemaData, { upsert: true }, function(err, doc) {
                    console.log('saving data ' + c++ + '--------------'+category.length);
                    console.log(doc)
                    if (err || doc === null) {
                        console.log('error in save category');
                        //console.log(CategorySchemaData)
                        Category.findOne({ term_id: CategorySchemaData.term_id }, function(err1, doc1) {
                            //console.log('doc')
                            //console.log(doc1)
                            categoryId.push(doc1._id)
                        })
                    } else {

                        console.log('category save successfully ' + doc._id);
                        categoryId.push(doc._id)
                    }
                    if (c >= category.length) {
                        impTag();
                    }
                });
            }

        } else {
            impTag();
        }

        function impTag() {
            if (postData.tag) {
                var tagC = 0,
                    tag = JSON.parse(postData.tag);
                if (tag.length <= 0) {
                    getAuthor();
                }
                for (i = 0; i < tag.length; i++) {
                    TagSchemaData = {
                        score: tag[i].count,
                        name: tag[i].name,
                        slug: slug(tag[i].slug)
                    }
                    Tag.findOneAndUpdate({ slug: slug(tag[i].slug) }, TagSchemaData, { upsert: true }, function(err, doc) {
                        tagC++;
                        if (err || doc === null) {
                            console.log('error in save tag');
                            Tag.findOne({ slug: slug(TagSchemaData.slug) }, function(err1, doc1) {
                                //console.log('doc')
                                //console.log(doc1)
                                tagsId.push(doc1._id)
                            })
                        } else {
                            console.log(doc)
                            console.log('save successfully  tag' + doc._id);
                            tagsId.push(doc._id)
                        }
                        if (tagC >= tag.length) {
                            getAuthor();
                        }

                    });
                }
            }
        }

        function getAuthor() {
            if (postData.users) {
                user = JSON.parse(postData.users);
                //console.log(user.data)
                //if (user.data) {
                var userDetails = user.data;
                var UserData = {
                    wp_ID: userDetails.ID,
                    display_name: userDetails.display_name,
                    email: userDetails.user_email,
                    password: userDetails.user_email,
                    wp_password: userDetails.user_pass,
                    username: userDetails.user_login,
                    website: userDetails.user_url,
                    wp_user_nicename: userDetails.wp_user_nicename,
                    user_status: userDetails.user_status,
                    wp_user_activation_key: userDetails.wp_user_activation_key,
                    gravatar: "https://www.gravatar.com/avatar/" + md5(userDetails.user_email.trim().toLowerCase()) + "?d=" + urlencode(userDetails.user_url) + "&s=200",
                    links: null,
                    location: null,
                    bio: null,
                    created_at: userDetails.user_registered
                };

                User.findOneAndUpdate({ wp_ID: userDetails.ID }, UserData, { upsert: true }, function(err, doc) {
                    if (!err) {
                        author_id = doc._id;
                        goahead()
                    } else {
                        //console.log(err)
                        User.findOne({ email: userDetails.user_email }, function(err1, doc1) {
                            author_id = doc1._id;
                            goahead();
                        })
                    }

                });
                // }
            }
        }

        function goahead() {
            // console.log(postData);
            //console.log(postData.users);
            //console.log(postMeta);
            //console.log(category);
            //console.log(tag);
            // console.log(postFeatureImages);
            //console.log(postFeaturevideo);
            //console.log(categoryId);
            //console.log(tagsId);
            console.log(postData);
            console.log(JSON.parse(postData.featurevideo));
            console.log(JSON.parse(postData.featurevideo).td_video);
            var comment_status = false;
            var ptype = 'default';
            var pstatus = false;
            if (posts.comment_status == 'open') { comment_status = true; };
            if (posts.post_status == 'publish') { pstatus = true; };
            if (postFeaturevideo !== '') { ptype = "video" };
            var ArticleData = {
                tags: tagsId,
                wp_meta_data: null,
                categories: categoryId,
                subTitle: null,
                deleted_at: null,
                wp_featuredImage: postFeatureImages,
                slug: slug(posts.post_name),
                _author: author_id,
                _editors: null,
                _medias: null,
                _featuredImage: null,
                comments: null, //todo limit to 3
                wp_ID: posts.ID,
                wp_post_author: posts.post_author,
                commentCount: posts.comment_count,
                commentStatus: comment_status,
                title: posts.post_title,
                body: posts.post_content,
                excerpt: posts.post_excerpt,
                content: null,
                published: pstatus,
                updated_at: posts.post_modified,
                created_at: posts.post_date,
                updatedAt: posts.post_modified_gmt,
                //createdAt: posts.post_date_gmt,
                post_status: posts.post_status,
                videourl: JSON.parse(postData.featurevideo).td_video,
                type: ptype,
                postType: ptype
            };
            console.log(ArticleData)
            Article.findOneAndUpdate({ wp_ID: posts.ID }, ArticleData, { upsert: true }, function(err, doc) {

                if (err) {
                    console.log(err)
                    return res.json({ status: false });
                } else {
                    console.log(doc)
                    return res.json({ success: true });
                }
            });
            // console.log(ArticleData)
        }



    }
    //console.log(JSON.parse(postData[0]));
    //res.json({ done: "Done" });
})

app.get('/wpsyncTrash/:postID', function(req, res) {
    postID = req.params.postID && parseInt(req.params.postID) != 0 ? Math.abs(req.params.postID) : 0;
    if (postID) {
        ArticleData = {
            published: false,
            post_status: 'trash'
        }
        Article.findOneAndUpdate({ wp_ID: postID }, ArticleData, { upsert: true }, function(err, doc) {

            if (err) {
                console.log(err)
                return res.json({ status: false });
            } else {
                console.log(doc)
                return res.json({ success: true });
            }
        });
    }
})
app.get('/wpsyncUntrash/:postID', function(req, res) {
        postID = req.params.postID && parseInt(req.params.postID) != 0 ? Math.abs(req.params.postID) : 0;
        if (postID) {
            ArticleData = {
                published: true,
                post_status: 'publish'
            }
            Article.findOneAndUpdate({ wp_ID: postID }, ArticleData, { upsert: true }, function(err, doc) {

                if (err) {
                    console.log(err)
                    return res.json({ status: false });
                } else {
                    console.log(doc)
                    return res.json({ success: true });
                }
            });
        }
    })
    // //PUBLISH
    // /*
    // __________     ___.   .__  .__       .__
    // \______   \__ _\_ |__ |  | |__| _____|  |__
    //  |     ___/  |  \ __ \|  | |  |/  ___/  |  \
    //  |    |   |  |  / \_\ \  |_|  |\___ \|   Y  \
    //  |____|   |____/|___  /____/__/____  >___|  /
    //                     \/             \/     \/
    // */
    //
    // app.put('/v1/articles/:id/publish', function(req, res){
    //
    // });
    //
    // //end publish

/*
 __            _        _
/ _\ ___   ___| | _____| |_ ___
\ \ / _ \ / __| |/ / _ \ __/ __|
_\ \ (_) | (__|   <  __/ |_\__ \
\__/\___/ \___|_|\_\___|\__|___/

  with socket.io
*/


//emit message through sockets when new article is posted

io.on('connection', function(socket) {
    console.log('a user connected');
    socket.on('disconnect', function() {
        console.log('user disconnected');
    });

    socket.on('article created', function(artcl) {
        io.emit('article created', artcl);
    });

    socket.on('article published', function(artcl) {
        io.emit('article published', artcl);
    });

});

//end chat


//listen
//config.port = 3300;
var server = http.listen(config.port, function() {
    console.log('listening on *:' + config.port);
});
//end listen

server.timeout = 10000;
