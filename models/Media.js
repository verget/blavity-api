var mongoose = require('mongoose');
var User = require('./User.js');
var Media = require('./Media.js');
var Article = require('./Article.js');
var soft_delete = require('mongoose-softdelete');



var MediaSchema = new mongoose.Schema({
  tags: Array,
  alt: String,
  type: String,
  _author : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  _article : { type: mongoose.Schema.Types.ObjectId, ref: 'Article' },
  title: String,
  caption: String,
  url: String,
  path: String,
  filename: String,
  updated_at: { type: Date, default: Date.now },
});

MediaSchema.plugin(soft_delete);

module.exports = mongoose.model('Media', MediaSchema);
