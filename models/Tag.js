var mongoose = require('mongoose');
var soft_delete = require('mongoose-softdelete');

var TagSchema = new mongoose.Schema({
  name: String,
  slug: String,
  score: Number,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  deleted_at: { type: Date, default: null }
});

TagSchema.plugin(soft_delete);

module.exports = mongoose.model('Tag', TagSchema);
