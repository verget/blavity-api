var mongoose = require('mongoose');
var Media = require('./Media.js');
var bcrypt = require('bcrypt-nodejs');
var soft_delete = require('mongoose-softdelete');
//var md5 = require('md5');


var UserSchema = new mongoose.Schema({
  wp_ID: String,
  username: {
      type: String,
      unique: true,
      required: true
  },
  full_name: {
        type: String,
  },
  wp_user_nicename: {type: String},
  email: {
        type: String,
        unique: true,
        required: true
    },
  password: {
        type: String,
        required: true,
        // select: false//so it isn't returned by default
    },
   wp_password: {
      type: String,
      required: true,
      // select: false//so it isn't returned by default
  },
  website: String,
  links: Array,
  wants_newsletter : Boolean,
  user_activation_key: {type: String},
  password_reset_key: {type: String},
  user_status: {type: String, default: 'inactive'},
  display_name: {type: String},
  location: String,
  bio: String,
  _avatar : { type: mongoose.Schema.Types.ObjectId, ref: 'Media' },
  gravatar: String,
  updated_at: { type: Date, default: Date.now },
  created_at: { type: Date, default: Date.now }
});


UserSchema.pre('save', function (next) {

    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function (err, salt) {
            if (err) {
                return next(err);
            }
            bcrypt.hash(user.password, salt,
                function(){

                },
                function (err, hash) {
                    if (err) {
                        return next(err);
                    }
                    user.password = hash;
                    next();
                });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function (passw, cb) {
    bcrypt.compare(passw, this.password, function (err, isMatch) {
        if (err) {
            return cb(err);
        }
        cb(null, isMatch);
    });
};


// UserSchema.methods.comparePassword = function (passw, cb) {

//     var user = this.findOne({ email: this.email }).select('+password').exec(function (err, user) {
//         bcrypt.compare(passw, user.password, function (err, isMatch) {
//             if (err) {
//                 return cb(err);
//             }
//             cb(null, isMatch);
//         });
//     });

// };

module.exports = mongoose.model('User', UserSchema);
