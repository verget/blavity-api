var mongoose = require('mongoose');
var User = require('./User.js');
var Media = require('./Media.js');
var Comment = require('./Comment.js');
var Category = require('./Category.js');
var shortid = require('shortid');
var Tag = require('./Tag.js');
var soft_delete = require('mongoose-softdelete');


function validateTags (tags, next){
  //truncate tags to the first three if more than that come in
  if (this.tags.length > 3) this.tags = this.tags.slice(0, 3);
  next();
}

function validateBody (body, next){
  //truncate tags to the first three if more than that come in
  if (this.body.trim() == '') return false;
  next();
}


/*var ArticleSchema = new mongoose.Schema({
  _tags: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tag' }],
  _categories: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
  slug: String,
  // _parent : { type: mongoose.Schema.Types.ObjectId, ref: 'Article' },
  _author : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  _editors : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  _medias : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Media' }],
  _featuredImage : { type: mongoose.Schema.Types.ObjectId, ref: 'Media' },
  wp_featuredImage : String,
  comments : Array,//todo limit to 3
  // comments : [Comment],//todo limit to 3
  commentCount : Number,
  commentStatus : Boolean,
  title: String,
  subTitle: String,
  body: { type: String, validate: validateBody},
  excerpt: String,
  content: Array,
  published: Boolean,
  submitted: Boolean,
  type: {type: String, enum: ['opinion','listicle', 'video', 'news','other']},
  listicle:{ type: Array, default: null },
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  deleted_at: { type: Date, default: null }
},
{
    timestamps: true
});*/
var ArticleSchema = new mongoose.Schema({
  wp_ID:  {
        type: String,
        unique: true,
        required: true,
        default: shortid.generate
    },
  wp_post_author: String,
  postType: String,
  wp_meta_data : { type: mongoose.Schema.Types.Mixed},
  tags: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Tag' }],
  categories: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Category' }],
  slug: {
        type: String,
        unique: true,
        required: true
    },
  // _parent : { type: mongoose.Schema.Types.ObjectId, ref: 'Article' },
  _author : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  _editors : [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }],
  _medias : [{ type: mongoose.Schema.Types.ObjectId, ref: 'Media' }],
  _featuredImage : { type: mongoose.Schema.Types.ObjectId, ref: 'Media' },
  comments : Array,//todo limit to 3
  wp_featuredImage:String,
  videourl:String,
  commentCount : Number,
  commentStatus : Boolean,
  title: String,
  subTitle: String,
  body: { type: String, validate: validateBody},
  excerpt: String,
  content: Array,
  type: {type: String, enum: ['opinion','listicle', 'video', 'news','other']},
  listicle:{ type: Array, default: null },
  published: Boolean,
  post_status: String,
  deleted_at: { type: Date, default: null },
  updated_at: { type: Date, default: Date.now },
  created_at: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  createdAt: { type: Date, default: Date.now },
},
{
    timestamps: true
});

ArticleSchema.plugin(soft_delete);

module.exports = mongoose.model('Article', ArticleSchema);


// // save pre
// ArticleSchema.pre('save', function (next) {
//   //count all the comments that belong to this article
//   Comment.count({ published: true }).where('_article').equals(this._id).sort({ _id : -1}).exec(function(err, count) {
//       if (err) throw err;

//       this.commentCount = count;
//   });

//   //get the first max 3 comments and push them to the subdocuments (for preview of the comment thread)
//   Comment.find({ published: true }).where('_article').equals(this._id).sort({ _id : -1}).limit(3).exec(function(err, comments) {
//       if (err) throw err;

//       this.comments = comments;
//   });

//   next();
// });
// //end save pre


// // update pre
// ArticleSchema.pre('update', function() {
//   this.update({},{ $set: { updated_at: new Date() } });
// });
// //end update pre







// //delete a comment from an article
// var comment = article.comments.id(comment._id).remove();
// article.save(function (err) {
//   if (err) return handleError(err);
//   console.log('the comment was removed');
// })
