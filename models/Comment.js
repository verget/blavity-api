var mongoose = require('mongoose');
// var User = require('./User.js');
var Article = require('./Article.js');
var Media = require('./Media.js');
var Comment = require('./Comment.js');
var soft_delete = require('mongoose-softdelete');


var CommentSchema = new mongoose.Schema({
  // slug: String,
  _author : { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  _article : { type: mongoose.Schema.Types.ObjectId, ref: 'Article' },
  _parent : { type: mongoose.Schema.Types.ObjectId, ref: 'Comment' },
  body: String,
  published: Boolean,
  created_at: { type: Date, default: Date.now },
  updated_at: { type: Date, default: Date.now },
  deleted_at: { type: Date, default: null },
},
{
    timestamps: true
});

CommentSchema.plugin(soft_delete);

module.exports = mongoose.model('Comment', CommentSchema);


// CommentSchema.pre('save', function (next) {
//   //just save the article since it already has a hook to update it's 3 latest comments

//   // Article.findOne({ _id: this._article}, function (err, article) {
//   //   if (err)
//   //     return next(err);

//   //   article.save();
//   //   next();
//   // });

// });

// CommentSchema.pre('validate', function (next) {
//   //truncate string if it's too long
//   //remove any characters that shouldn't be there
//   next();
// });
