var Article = require('../models/Article');
var User = require('../models/User');
var Comment = require('../models/Comment');
var mongoose    = require('mongoose');//mongodb orm
var searchIndex = require('search-index');
var assert = require('assert');
var casual = require('casual');
var count=1;
var slug="top-10-reasons-to-read-documentation-first";
var articles_id='57738acdc5ffc26a5121679a';
var date_range="2016-06-29T14:43:42+05:30 , 2016-06-29T15:28:15+05:30";
var username='TestTest';
var user_id='577371048b70ef5d3813447d';
var userEmail = "jdtheman@coolmail.com";
var options={};
var new_article_id='';
var new_comment_id='';

var commentObj = {
  body: casual.sentence,
}
//mongoose.connect('mongodb://localhost/article');
describe('Article', function() {
  var articleObj ={
                    tags: "development, programming, coding",
                    categories: "tech, business",
                    slug: "top-10-reasons-to-read-documentation-first",
                    title: "Top 10 Reasons to Read the Documentation First",
                    subTitle: "Hopefully It Will Help",
                    commentStatus: true,
                    published: true,
                    _author: "577371048b70ef5d3813447d",
                    body: "Lorem ipsum dolor sit amet...",
                    excerpt: "Lorem ipsum dolor sit amet..."
                  }
      //valid request
      describe('#save()', function() {
        it('should save article without error', function(done) {
          var newArticle = new Article(articleObj);
          newArticle.save(function(err,article) {
            if(!err){
              assert.equal(err, null, "Article is created");
              new_article_id=article._id;
            }

            done();
          });
        });
      });

      describe('#list()', function() {
        it('should list latest article without error', function(done) {
          var q = Article.find({}).sort({'_id': -1}).limit(count).populate("_medias _author");
          // var q = Article.find({ published: true }).sort({'_id': -1}).limit(count).populate("_medias");
          q.exec(function(err, articles) {
              if (!err)
                  assert.equal(articles!=null, true, "Article is listed");
              done();
            });
          });


          it('should list article using slug without error', function(done) {
            var q =  Article.findOne({ slug: slug}).populate('_medias _author');
            // var q = Article.find({ published: true }).sort({'_id': -1}).limit(count).populate("_medias");
            q.exec(function(err, articles) {
                if (!err)
                  assert.equal(articles != null, true, "Article is listed with slug");
                done();
              });
            });


            it('should list related article using  without error', function(done) {
              var q =  Article.findOne({ slug: slug}).populate('_medias _author');
              // var q = Article.find({ published: true }).sort({'_id': -1}).limit(count).populate("_medias");
              q.exec(function(err, articles) {
                  if (!err)
                  {
                        var tags = articles.tags;
                        var qr=Article.find({ tags: { $in : tags }, _id: { $nin: [ articles._id] }  }).sort({ _id : -1}).limit(count).populate('_medias _author');
                        qr.exec(function(err,article){
                            if(!err){
                                  assert.equal(article != null, true, "related Article is listed with limit ");
                            }
                            else {
                              assert.equal(err !== null, true, " Error is occured");
                            }
                              done();
                        })
                  }
                  else{
                      assert.equal(err !== null, true, " Error is occured");
                      done();
                  }
                });
              });


              it('should list next article using slug without error', function(done) {
                var q =  Article.findOne({ slug: slug}).populate('_medias _author');
                // var q = Article.find({ published: true }).sort({'_id': -1}).limit(count).populate("_medias");
                q.exec(function(err, articles) {
                    if (!err){
                      var qr=Article.find({}).where('_id').lt(articles._id).sort({ _id : -1}).limit(1).populate('_medias _author');
                      qr.exec(function(err,article){
                          if(!err){
                                assert.equal(article != null, true, "Successful next article using slug without error");
                          }
                          else {
                            assert.equal(err !== null, true, " Error is occured");
                          }
                            done();
                      })
                    }
                    else{
                        assert.equal(err !== null, true, " Error is occured");
                        done();
                    }
                  });
                });

                it('should list next article using id without error', function(done) {
                    var qr=Article.find({}).where('_id').lt(articles_id).sort({ _id : -1}).limit(1).populate('_medias _author');
                    qr.exec(function(err,article){
                        if(!err){
                              assert.equal(article != null, true, "Successful next article using slug without error");
                        }
                        else {
                          assert.equal(err !== null, true, " Error is occured");
                        }
                          done();
                    })
                });


                it('should list previous article using slug without error', function(done) {
                  var q =  Article.findOne({ slug: slug}).populate('_medias _author');
                  // var q = Article.find({ published: true }).sort({'_id': -1}).limit(count).populate("_medias");
                  q.exec(function(err, articles) {
                      if (!err){
                        var qr=Article.find({}).where('_id').gt(articles._id).sort({ _id : -1}).limit(1).populate('_medias _author');
                        qr.exec(function(err,article){
                            if(!err){
                                  assert.equal(article != null, true, "Successful previous article using slug without error");
                            }
                            else {
                              assert.equal(err !== null, true, " Error is occured");
                            }
                              done();
                        })
                      }
                      else{
                          assert.equal(err !== null, true, " Error is occured");
                          done();
                      }
                    });
                  });

                  it('should list previous article using id without error', function(done) {
                      var qr=Article.find({}).where('_id').gt(articles_id).sort({ _id : -1}).limit(1).populate('_medias _author');
                      qr.exec(function(err,article){
                          if(!err){
                                assert.equal(article != null, true, "Successful previous article using slug without error");
                          }
                          else {
                            assert.equal(err !== null, true, " Error is occured");
                          }
                            done();
                      })
                  });

                  it('should list any article within Date range using id without error', function(done) {
                      if(date_range){
                        var range = date_range;
                        range.replace(/ /g,''); //remove spaces
                        range = range.split(','); //turn into array separated by commas
                        var start = new Date(range[0]);
                        var end = new Date(range[1]);
                        //set query between the ranges
                        query = {"created_at": {$gte: start, $lt: end}};
                      }//end if date range
                      else{
                        query = {};
                      }
                      Article.count(query).exec(function(err, count){
                        var random = Math.floor(Math.random() * count);
                        Article.findOne().skip(random).populate('_medias _author').exec(
                          function (err, article) {
                            if(!err){
                                  assert.equal(article != null, true, "Successful list any article within Date range using id without error");
                            }
                            else {
                              assert.equal(err !== null, true, " Error is occured");
                            }
                              done();
                        });
                      });
                  });



                  it('should list all articles by an author with limit', function(done) {
                      //get user by username
                      User.findOne({ username: username}, function (err, user) {
                        if (err){
                            assert.equal(err !== null, true, " Error is occured");
                        }
                        else{
                          Article.find({ published: true, _author: user._id }).sort({'_id': -1}).limit(count).exec(function(err, articles) {
                              if (err)
                                  assert.equal(err !== null, true, " Error is occured");
                              else
                                  assert.equal(articles != null, true, "Successful get all articles by an author");

                              done();
                          });//end fine artcles
                        }

                      });//end find user by username
                  });



                  it('should get author by username', function(done) {
                      //get user by username
                      User.findOne({ username: username}, function (err, user) {
                        if (err)
                            assert.equal(err !== null, true, " Error is occured");
                        else
                                  assert.equal(user != null, true, "Successful get author by username");
                          done();
                      });//end find user by username
                  });

                  it('should get author by user id', function(done) {
                      //get user by username
                      User.findOne({ _id: user_id}, function (err, user) {
                        if (err)
                            assert.equal(err !== null, true, " Error is occured");
                        else
                                  assert.equal(user != null, true, "Successful get author by user id");
                          done();
                      });//end find user by username
                  });

                  it('should add a comment to a article',function(done){
                      var comment = new Comment(commentObj);
                    User.findOne({
                      email: userEmail
                    }, function(err, user) {
                        //if (err) throw err;
                        //console.log(user);
                        if(err)
                        assert.equal(err, true, "An error occured in finding user");
                        //if user does not exists
                        assert.equal(user!=null, true, "Authentication failed. User not found.");

                        //get the comment as it was just posted and make a mongo object out of it
                        var comment = new Comment(commentObj);
                        //set comment to published so it's visible by default for now
                        comment.published = true;
                        //set the comment's author id
                        comment._author = user._id;
                        comment._article=articles_id;

                        // save the comment
                        comment.save(function (err, comment) {
                          //if (err) return handleError(err);
                          if(err)
                          assert.equal(err == null, true, "An error occured in saving commet");
                          new_comment_id=comment._id;
                          //todo:
                          //move this to the schema when you get the pres working
                          Article.findOne({ '_id' : articles_id }, function (err, article) {
                            assert.equal(err==null, true, "An error occured in finding article");
                            //count all the comments that belong to this article
                            Comment.count({ published: true }).where('_article').equals(article._id).exec(function(err, count) {
                              //if (err) throw err;
                              assert.equal(err==null, true, "An error occured in finding count of comments");
                              article.commentCount = count;
                              //get the first max 3 comments and push them to the subdocuments (for preview of the comment thread)
                              Comment.find({ published: true }).where('_article').equals(article._id).sort({ _id : -1}).limit(3).exec(function(err, comments) {
                                //if (err) throw err;
                                assert.equal(err==null, true, "An error occured in retriving first 3 comments");
                                article.comments = comments;
                                article.save();
                                done();
                              });

                            });
                          });
                          });
                  });//end save comment
                  });


                  it('should get an articles n latest comments', function(done) {
                      //get user by username
                      User.findOne({ _id: user_id}, function (err, user) {
                        if (err)
                            assert.equal(err !== null, true, " Error is occured");
                        else
                                  assert.equal(user != null, true, "Successful get author by user id");
                          done();
                      });//end find user by username
                  });


                  it('should get an articles n latest comments by authors', function(done) {
                    var q = Comment.find({ published: true, _author: user_id }).sort({'_id': -1}).limit(count);

                    q.exec(function(err, comments) {
                        if (err)
                            assert.equal(err !== null, true, " Error is occured");
                        else {
                            assert.equal(comments != null, true, "Successful get articles n latest comments by authors");
                        }
                        done();
                    });
                  });


                  it('should search an articles', function(done) {
                      searchIndex(options, function(err, si) {
                      q = {};
                       q.query = {'*': ['test']}; //search for string in all ('*') fields
                        si.search(q, function (err, searchResults) {
                          //do something with searchResults
                          done();
                        });
                      });
                  });
        });



        describe('#update()', function() {
          it('should Update article without error', function(done) {
            Article.findByIdAndUpdate(articles_id, articleObj, function (err, article) {
              if (err)
                  assert.equal(err !== null, true, " Error is occured");
              else {
                  assert.equal(article!=null, true, "Article is listed");
              }
              done();

            });
          });
        });



        describe('#delete()', function() {
          it('should delete article without error', function(done) {
              Article.findById(user_id, function (err, article) {
                if (err)
                  assert.equal(err !== null, true, " Error is occured");
                if ( String(articleObj._author) == String(user_id)) {//if user is found and is the author of the article
                  var realdeletefromanadmin = true;
                  //this actually deletes models from the db
                  if(realdeletefromanadmin){
                    //if the logged in user has proper permissions
                    //find the article by id and soft delete it
                    Article.findByIdAndRemove(new_article_id, articleObj, function (err, article) {
                      if (err)   assert.equal(err !== null, true, " Error is occured");
                      //remove the article from the search index
                      searchIndex(options, function(err, si) {
                        si.del(new_article_id, function(err) {
                          if (!err) assert.equal(err == null, true, "Article is softdelete");
                          else assert.equal(err !== null, true, " Error is occured");
                        });
                      });
                    });
                  }//end real deletes
                  else{ //do a soft delete that can be restored (like a user changing their mind later)
                    Article.softdelete(function(err, arcl) {
                      if (!err) assert.equal(err == null, true, "Article is softdelete");
                      else assert.equal(err !== null, true, " Error is occured");
                    });
                  }// end else
                  done();
                }
              });//end findById
          });


          it('should delete articles comments without error', function(done) {
            Comment.findByIdAndRemove(new_comment_id, commentObj, function (err, comment) {
              if (err) assert.equal(err !== null, true, " Error is occured");
              else {
                assert.equal(comment !== null, true, "Successfull delete articles comments without error");
              }
              done();
            });
          });

        });
});
