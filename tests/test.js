var supertest = require("supertest");
var should = require("should");
var casual = require('casual');
var server = supertest.agent("http://localhost:5000");
var count=1;
var token='';
var slug="top-10-reasons-to-read-documentation-first";
var articles_id='';
var comment_id='';
var user_id='577371048b70ef5d3813447d';
var date_range="2016-06-29T14:43:42+05:30 ,2016-06-30T09:12:55.557Z";
var username='TestTest';
var commentObj = {
  body: casual.sentence,
  _article:articles_id
}

var articleObj ={
                //  _id:"57738ae0f74ebba551d55de0",
                  tags: "development, programming, coding",
                  categories: "tech, business",
                  slug: "top-10-reasons-to-read-documentation-first",
                  title: "Top 10 Reasons to Read the Documentation First",
                  subTitle: "Hopefully It Will Help",
                  commentStatus: false,
                  published: true,
                  _author: "5773b949ffa7fa720ba32a2b",
                  body: "Lorem ipsum dolor sit amet...",
                  excerpt: "Lorem ipsum dolor sit amet..."
                };


// UNIT test begin
describe("Authentication Api Unit test",function(){
  // #1 should return home page
  it("should not login with unregister users ",function(done){
    server
    .post("/v1/auth/authenticate/")
    .send({user:{email:casual.email,password:casual.password}})
    .set('Content-type', 'application/json')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      // Error key should be false.
    //  res.body.error.should.equal(false);
      done();
    });
  });

  it("should login with unregister users ",function(done){
    server
    .post("/v1/auth/authenticate/")
    .send({user:{email:"Frami.Lelah@hotmail.com",password:"asdfg1234"}})
    .set('Content-type', 'application/json')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      // Error key should be false.
    //  res.body.error.should.equal(false);
      token=res.body.token;
    /*  var parted = res.body.token.split(' ');
      if (parted.length === 2) {
        token=parted[1];
      }*/
      done();
    });
  });

  it("should register with new users ",function(done){
    var userObj = {
      "username": casual.username,
      "email": casual.email,
      "password": "asdfg1234"
    }
    server
    .post("/v1/auth/signup/")
    .send({user:userObj})
    .set('Content-type', 'application/json')
    .expect("Content-type",/json/)
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      // Error key should be false.
      res.body.success.should.equal(true);
      done();
    });
  });
});



describe("insert Api Unit test",function(){
  it("should insert update article",function(done){
    server
  .post("/v1/articles/")
  .set('Content-type', 'application/json')
  .set('token', token)
  .send({article:articleObj})
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
        console.log(res.body._id);
      if(res.body.success==false)
        res.status.should.equal(403);
      else
      {
        res.status.should.equal(200);
        articleObj._id=res.body._id;
        articles_id=res.body._id;
        commentObj._article=articles_id;
        articleObj.commentStatus= true;
      }
      // HTTP status should be 200`
      done();
    });
  });

  // #1 should return home page
  it("should add comment to article",function(done){
    server
  .post("/v1/articles/comments")
  .set('Content-type', 'application/json')
  .set('token', token)
  .send({comment:commentObj})
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      if(res.body.success==false)
        res.status.should.equal(403);
      else{
        res.status.should.equal(200);
        comment_id=res.body.comment._id;
      }
      // HTTP status should be 200`
      done();
    });
  });
});



describe("Artical Reading Api Unit test",function(){
  // #1 should return home page
  it("should read artical by slug ",function(done){
    server
    .get("/v1/"+slug+"/")
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200
      res.status.should.equal(200);
      // Error key should be false.
    //  res.body.error.should.equal(false);
      done();
    });
  });

  it("should read n latest artical",function(done){
    server
    .get("/v1/articles/"+count)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should read n related artical",function(done){
    server
    .get("/v1/related/"+slug+"/"+count)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should read next artical by id",function(done){
    server
    .get("/v1/articles/next/"+articles_id)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });


  it("should read next artical by slug",function(done){
    server
    .get("/v1/articles/next/"+slug+'-1')
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      console.log(res.body);
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should read previous artical by id",function(done){
    server
    .get("/v1/articles/previous/"+articles_id)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should read previous artical by slug",function(done){
    server
    .get("/v1/articles/previous/"+slug+'-2')
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      console.log(res.body);
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should read any artical bitween given date range",function(done){
    server
    .get("/v1/articles/any/"+date_range)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      //console.log(res);
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should read n artical of given authors",function(done){
    server
    .get("/v1/articles/authors/"+username+"/"+count)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  it("should get authors by username",function(done){
    server
    .get("/v1/authors/"+user_id)
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });

  /*it("should search any artical based on text",function(done){
    server
    .get("/v1/articles/search/test")
    .set('Content-type', 'application/json')
    .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      console.log(err);
      // HTTP status should be 200`
      res.status.should.equal(200);
      done();
    });
  });*/
});


describe("update Api Unit test",function(){
  it("should get update article",function(done){
    server
  .put("/v1/articles/")
  .set('Content-type', 'application/json')
  .set('token', token)
  .send({article:articleObj})
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      if(res.body.success==false)
        res.status.should.equal(403);
      else
      res.status.should.equal(200);
      // HTTP status should be 200`
      done();
    });
  });
});



describe("Delete Api Unit test",function(){
  // #1 should return home page
  it("should delete article",function(done){
    server
  .delete("/v1/articles/"+articleObj._id)
  .set('Content-type', 'application/json')
  .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      if(res.body.success==false)
        res.status.should.equal(403);
      else
      res.status.should.equal(200);
      // HTTP status should be 200`
      done();
    });
  });

  it("should delete articles comment",function(done){
    server
  .delete("/v1/articles/comments/"+comment_id)
  .set('Content-type', 'application/json')
  .set('token', token)
    .expect("Content-type",'application/json')
    .expect(200) // THis is HTTP response
    .end(function(err,res){
      if(res.body !=null &&res.body.success==false)
        res.status.should.equal(403);
      else
      res.status.should.equal(200);
      // HTTP status should be 200`
      done();
    });
  });
});
