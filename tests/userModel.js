var User = require('../models/User.js');
var mongoose    = require('mongoose');//mongodb orm
var assert = require('assert');
var casual = require('casual');
mongoose.connect('mongodb://localhost/article');

describe('User', function() {
  var userObj = {
    "username": casual.username,
    "email": casual.email,
    "password": "asdfg1234"
  }
  //valid request
  describe('#Signup()', function() {
    it('should Signup without error', function(done) {
      var newUser = new User(userObj);
      newUser.save(function(err) {
        assert.equal(err, null, "User not created");
        done();
      });
    });

    //invalid request
    it('should not Signup user as duplicate keys', function(done) {
      var newUser = new User(userObj);
      newUser.save(function(err) {
        assert.notEqual(err, null, "User has been created");
        done();
      });
    });

  });


  describe('#login()', function() {
    it('should login without error', function(done) {
      User.findOne({
        email: userObj.email
      }, function(err, user) {
        //if err

        assert.equal(err, null, "An error occured");
        //if user does not exists
        assert.equal(user != null, true, "Authentication failed. User not found.");
        //If user exists check if password matches
        user.comparePassword(userObj.password, function (err, isMatch) {
          //if (isMatch && !err)
          assert.equal(isMatch && !err, true, "Authentication failed. Wrong password.");
          assert.equal(isMatch, true, "Authentication success.");
          done();
        });
      });
    });

    //invalid request
    it('should not login user as invalid data', function(done) {
      User.findOne({
        "email": userObj.email
      }, function(err, user) {
        assert.equal(err, null, "An error occured");
        //if user does not exists
        assert.equal(user != null, true, "Authentication failed. User not found.");
        //If user exists check if password matches
        //Creating invalid password
        user.comparePassword(userObj.password + "asdasd", function (err, isMatch) {
          if(isMatch){
              assert.equal(isMatch, true, "Authentication success.");
          }
          else{
              assert.equal(!isMatch, true, "Authentication failed. Wrong password.");
          }
          done();
        });
      });
    });

  });

});
