var nodemailer = require('nodemailer');
var sgTransport = require('nodemailer-sendgrid-transport');
var config = require('../config/config');
var User = require('../models/User.js');
var _this = this;

var options = {
    auth: {
        api_user: config.mail.api_user,
        api_key: config.mail.api_key
    }
};

var client = nodemailer.createTransport(sgTransport(options));

_this.templates = {
    verification: {
        subject: 'Verification mail from Blavity',
        text: 'Verification mail from Blavity',
        html: function(user){return "<a href='" + config.mail.sender_domain + "/mail/verification/" + user.user_activation_key +"'> It's your verification link </a>"},
    },
    reset: {
        subject: 'Reset password mail from Blavity',
        text: 'Reset password mail from Blavity',
        html: function(user){return "<a href='" + config.mail.sender_domain + "/mail/reset-password/" + user.password_reset_key +"'> It's your password reset link </a>"},
    }
};

_this.sendMail = function(type, userId, cb) {
    User.findOne({
        _id: userId
    }, function(err, user) {
        if (err) cb(err);
        if (!user) {
            cb("User not found.");
        } else {
            var emailObject = {
                from: config.mail.sender_email,
                to: user.email,
                subject: _this.templates[type].subject,
                text: _this.templates[type].text,
                html: _this.templates[type].html(user)
            };
            client.sendMail(emailObject, function (err, info) {
                if (err) {
                    cb('Mail Sending ' + err);
                } else {
                    cb(null, 'Message sent: ' + info.message);
                }
            });
        }
    });
};

module.exports = _this;