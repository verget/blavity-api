var config = {
  port: 3100,
  secret: 'blavityIsAwesome',
  database: 'mongodb://localhost/blavity',
  mail: {
    api_user: 'dmitry.blavity',
    api_key: 'blavity1',
    sender_email: 'mail@blavity.com',
    sender_domain: 'http://preview.blavity.com'
  }
};

module.exports = config;
